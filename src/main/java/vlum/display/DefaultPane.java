package vlum.display;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;



import vlum.ContextData;
import vlum.graph.*;

/**
 * A slider for manipulating the <i>standard</i> value.
 * Creation date: (2/04/2003 12:39:17 PM)
 * @author: Andrew Lum
 */
public class DefaultPane implements ActionListener, ChangeListener {

	private ContextData myContextData;

	// some graph fields to be supplied by caller
	private GraphSetup myGraphSetup;	
	private int myCurrentDisplayIndex;
	
	// display panels to be supplied by caller
    private JPanel myContentPane;
	private SquidgePane mySquidgePane;

	// display panels created by this class
    private JPanel myControlPane;
	 
    // the standard slider
    private JSlider myMarkSepSlider;
    private JLabel myMarkSepLabel;

    // display values
    private int myMarkSepValue;

/**
 * StandardSlider constructor comment.
 */
public DefaultPane(GraphSetup graphSetup, int currentDisplayIndex,JPanel contentPane, SquidgePane squidgePane, ContextData contextData) {
	// store graph fields
	myGraphSetup = graphSetup;
	myCurrentDisplayIndex = currentDisplayIndex;
	myContextData = contextData;
	
	// set or create display panes
	myContentPane = contentPane;
	mySquidgePane = squidgePane;
	myControlPane = new JPanel();

	JButton myBackButton = new JButton("Back");
	myBackButton.setActionCommand("back");
	myBackButton.addActionListener(this);

	myControlPane.add(myBackButton);

	myControlPane.setLayout(new BoxLayout(myControlPane, BoxLayout.X_AXIS));
    JPanel labelPane = new JPanel();

    labelPane.setLayout(new BoxLayout(labelPane, BoxLayout.Y_AXIS));
    labelPane.setMinimumSize(new Dimension(180, 30));

    myContentPane.add(myControlPane);
    
    myMarkSepValue = 50;

    myMarkSepSlider = new JSlider(0, 100);
    myMarkSepSlider.addChangeListener(this);
    myMarkSepLabel = new JLabel();
	setSepLabel();
    
    myControlPane.add(myMarkSepSlider);

    Dimension minSize = new Dimension(5, 20);
    Dimension prefSize = new Dimension(5, 20);
    Dimension maxSize = new Dimension(10, 20);

    labelPane.add(myMarkSepLabel);
    
    myControlPane.add(new Box.Filler(minSize, prefSize, maxSize));

    myControlPane.add(labelPane);
}
/**
 * Change the slider and label value.
 * Creation date: (2/04/2003 3:08:15 PM)
 */
public void changeValue(int value) {
    myMarkSepValue = 50;
    myMarkSepSlider.setValue(myMarkSepValue);
    setSepLabel();
	
}
/**
 * Set the label to be displayed.
 * Creation date: (2/04/2003 3:08:15 PM)
 */
private void setSepLabel() {
    int val = myMarkSepValue;
    if (myGraphSetup.isComparison(myCurrentDisplayIndex)) {
        myMarkSepLabel.setText("Standard is " + ((val >= 50) ? (((val - 50) * 2) + "% Above Average") : (((50 - val) * 2) + "% Below Average")));
    } else {
        myMarkSepLabel.setText("Standard is " + val + "%");
    }
}
/**
 * Change the visibility of the slider.
 * Creation date: (2/04/2003 3:08:15 PM)
 */
public void setVisible(boolean visible) {
      myMarkSepSlider.setEnabled(visible);
      myMarkSepLabel.setVisible(visible);
	
}

public void actionPerformed(ActionEvent e) {
	if ("back".equals(e.getActionCommand())) {
		if (myContextData.getHistorySize() > 1) {
			mySquidgePane.setSelectedTopic(myContextData.popHistoryItem());
		}
	}
} 

	/**
	 * Invoked when the target of the listener has changed its state.
	 * @param e  a ChangeEvent object
	 */
public void stateChanged(javax.swing.event.ChangeEvent e) {
    JSlider source = (JSlider) e.getSource();
	if (source == myMarkSepSlider) {
    	int val = (int) source.getValue();
    	if (val == myMarkSepValue) {
    	    return;
    	}
    	myMarkSepValue = val;
    	setSepLabel();
    	mySquidgePane.setMarkSeparationValue(val / 100f);
    }
}
}
