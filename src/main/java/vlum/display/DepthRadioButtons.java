package vlum.display;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import vlum.ContextData;

/**
 * A slider for manipulating the <i>depth</i> value.
 * Creation date: (2/04/2003 12:39:17 PM)
 * @author: Andrew Lum
 */

public class DepthRadioButtons implements ActionListener {

	// display panels to be supplied by caller
	private JPanel myContentPane;
	private SquidgePane mySquidgePane;

	// display panels created by this class
	private JPanel myControlPane;

	// the depth slider
	private JLabel myDepthLabel;

	private int depthLimit = -1;

	private ContextData myContext;

	/**
	 * DepthSlider constructor comment.
	 */
	public DepthRadioButtons(JPanel contentPane, SquidgePane squidgePane) {

		// set or create display panes
		myContentPane = contentPane;
		mySquidgePane = squidgePane;
		myControlPane = new JPanel();

		myControlPane.setLayout(new BoxLayout(myControlPane, BoxLayout.X_AXIS));
		JPanel labelPane = new JPanel();

		labelPane.setLayout(new BoxLayout(labelPane, BoxLayout.Y_AXIS));

		myDepthLabel = new JLabel();

		Dimension minSize = new Dimension(5, 20);
		Dimension prefSize = new Dimension(5, 20);
		Dimension maxSize = new Dimension(10, 20);

		myControlPane.add(new Box.Filler(minSize, prefSize, maxSize));
		myControlPane.add(labelPane);
		myDepthLabel.setText("Term Expansion");
		labelPane.add(myDepthLabel);
		myControlPane.add(new Box.Filler(minSize, prefSize, maxSize));

		myContentPane.add(myControlPane);

		ButtonGroup group = new ButtonGroup();
		JPanel radioPanel = new JPanel(new FlowLayout());
		radioPanel.setMaximumSize(new Dimension(350, 30));
		JRadioButton button1 = new JRadioButton("Less ");
		button1.setHorizontalTextPosition(JLabel.LEFT);
		group.add(button1);
		radioPanel.add(button1);
		button1.addActionListener(this);
		button1.setActionCommand("1");

		JRadioButton button2 = new JRadioButton("", true);
		group.add(button2);
		radioPanel.add(button2);
		button2.addActionListener(this);
		button2.setActionCommand("2");

		JRadioButton button3 = new JRadioButton("");
		group.add(button3);
		radioPanel.add(button3);
		button3.addActionListener(this);
		button3.setActionCommand("3");

		JRadioButton button4 = new JRadioButton("");
		group.add(button4);
		radioPanel.add(button4);
		button4.addActionListener(this);
		button4.setActionCommand("4");

		JRadioButton button5 = new JRadioButton(" More");
		group.add(button5);
		radioPanel.add(button5);
		button5.addActionListener(this);
		button5.setActionCommand("5");

		myControlPane.add(radioPanel);

		if (depthLimit == -1) {
			depthLimit = 10;
		}

		myControlPane.setBackground(new Color(236, 233, 216));	//windows beige
		myContentPane.setBackground(new Color(236, 233, 216));	//windows beige
		myDepthLabel.setBackground(new Color(236, 233, 216));	//windows beige
		labelPane.setBackground(new Color(236, 233, 216));	//windows beige
		radioPanel.setBackground(new Color(236, 233, 216));	//windows beige
		button1.setBackground(new Color(236, 233, 216));	//windows beige
		button2.setBackground(new Color(236, 233, 216));	//windows beige
		button3.setBackground(new Color(236, 233, 216));	//windows beige
		button4.setBackground(new Color(236, 233, 216));	//windows beige
		button5.setBackground(new Color(236, 233, 216));	//windows beige
	}
	/**
	 * Invoked when an action occurs.
	 */
	public void actionPerformed(java.awt.event.ActionEvent e) {
		Object source = e.getSource();
		String depth = ((JRadioButton) source).getActionCommand();
		setDepthLimit(Integer.parseInt(depth));
		//int myLength = myContext.getSelected().length;
		if (mySquidgePane.getCurrentSelected() == -1) {
			mySquidgePane.setSelectedTopic(myContext.getSelected());		
		} else {
			mySquidgePane.setSelectedTopic(mySquidgePane.getSelectedTopic());
			myContext.addClickstream("DEPTHCHANGE-" + depth);
		}
	}

	public void setDepthLimit(int val) {
		mySquidgePane.setDepthLimit(val);
	}
	
	public void setContext(ContextData con) {
		myContext = con;
	}
}
