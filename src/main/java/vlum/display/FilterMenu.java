package vlum.display;


import java.awt.event.*;
import java.util.*;


import javax.swing.*;


import vlum.*;


/**
 * The <i>Action</i> menu allows actions to be taken out on a
 * particular selected concept.
 * Creation date: (3/04/2003 3:57:53 PM)
 * @author: Andrew Lum
 */
public class FilterMenu extends GenericMenu implements ActionListener {

JMenuItem backMenuItem;
private JRadioButtonMenuItem filterMenuItemNone;

/**
 * ActionMenu constructor comment.
 */
public FilterMenu(SquidgePane squidgePane, ContextData contextData) {
    mySquidgePane = squidgePane;
    myContextData = contextData;
    
	menuItemNames = new Hashtable();
    
	myMenu = new JMenu("Filter");
	
	ButtonGroup filterGroup = new ButtonGroup();

    filterMenuItemNone = new JRadioButtonMenuItem("None");
    filterGroup.add(filterMenuItemNone);
    filterMenuItemNone.addActionListener(this);
    filterMenuItemNone.setSelected(true);
    myMenu.add(filterMenuItemNone);
    menuItemNames.put("None", new Integer(-2));	//-2

    for (int i = 0; i < myContextData.getSetup().relationshipsLength(); i++) {
        if (myContextData.getSetup().isViewable(i)) {
            JMenuItem filterMenuItem = new JRadioButtonMenuItem(myContextData.getSetup().getRelationshipType(i));
            filterMenuItem.addActionListener(this);
            myMenu.add(filterMenuItem);
            filterGroup.add(filterMenuItem);
            menuItemNames.put(myContextData.getSetup().getRelationshipType(i), new Integer(-2));	//-2
        }
    }
    if (myContextData.getSetup().relationshipsLength() <= 1) {
        myMenu.setEnabled(false);
    }

}
/**
 * Invoked when an action occurs.
 */
public void actionPerformed(java.awt.event.ActionEvent e) {
    Object source = e.getSource();
    String t = null;
    try {
        if (source.getClass() == Class.forName("javax.swing.JMenuItem"))
            t = ((JMenuItem) source).getText();
        else
            t = e.getActionCommand();
    } catch (ClassNotFoundException ex) {
        t = e.getActionCommand();
    }
    if (menuItemNames.get(t) != null) {
        // Chage Relationships Filter
        if (t.equals("None")) {
            mySquidgePane.setRelationshipFilter(null);
        } else {
            mySquidgePane.setRelationshipFilter(t);
        }
        mySquidgePane.setSelectedTopic(mySquidgePane.getSelectedTopic());
    }

}
/**
 * Disable the back button.
 * Creation date: (16/04/2003 11:40:12 AM)
 */
public void disableBack() {
	backMenuItem.setEnabled(false);
	}
/**
 * Enable the back button.
 * Creation date: (16/04/2003 11:40:12 AM)
 */
public void enableBack() {
	backMenuItem.setEnabled(true);
	}
/**
 * Insert the method's description here.
 * Creation date: (24/06/2003 11:58:30 AM)
 */
public void selectNone() {
filterMenuItemNone.setSelected(true);	
}
}
