package vlum.display;

import java.awt.event.*;
import javax.swing.*;

import vlum.*;

public class ActionPopupMenu extends GenericMenu implements ActionListener {
	private JPopupMenu popupMenu;

	public ActionPopupMenu(SquidgePane squidgePane, ContextData contextData) {
		mySquidgePane = squidgePane;
		myContextData = contextData;

		// Create some menu items for the popup
		JMenuItem menuFileNew = new JMenuItem("New");
		JMenuItem menuFileOpen = new JMenuItem("Open...");
		JMenuItem menuFileSave = new JMenuItem("Save");
		JMenuItem menuFileSaveAs = new JMenuItem("Save As...");
		JMenuItem menuFileExit = new JMenuItem("Exit");

		// Create a popup menu
		popupMenu = new JPopupMenu("Menu");
		popupMenu.add(menuFileNew);
		popupMenu.add(menuFileOpen);
		popupMenu.add(menuFileSave);
		popupMenu.add(menuFileSaveAs);
		popupMenu.add(menuFileExit);

		mySquidgePane.add(popupMenu);

		// Action and mouse listener support
		//enableEvents( AWTEvent.MOUSE_EVENT_MASK );
		menuFileNew.addActionListener(this);
		menuFileOpen.addActionListener(this);
		menuFileSave.addActionListener(this);
		menuFileSaveAs.addActionListener(this);
		menuFileExit.addActionListener(this);
	}

	public void processMouseEvent(MouseEvent event) {
		if (event.isPopupTrigger()) {
			popupMenu.show(event.getComponent(), event.getX(), event.getY());
		}
		//super.processMouseEvent( event );
	}

	public void actionPerformed(ActionEvent event) {
		// Add action handling code here
		System.out.println(event);
	}

}
