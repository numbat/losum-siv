package vlum.display;


import java.awt.event.*;
import java.util.*;

import java.net.*;


import javax.swing.*;


import vlum.*;



/**
 * The <i>Help</i> menu contains items that provide assistance on
 * the tool for the user.
 * Creation date: (3/04/2003 12:37:31 PM)
 * @author: Andrew Lum
 */
public class HelpMenu extends GenericMenu implements ActionListener {

	
/**
 * Create a new Help menu.
 * Creation date: (3/04/2003 12:39:51 PM)
 */
public HelpMenu(SquidgePane squidgePane, ContextData contextData) {
    mySquidgePane = squidgePane;
    myContextData = contextData;
    
    // setup the draw menu
    myMenu = new JMenu("Help");

	menuItemNames = new Hashtable();
    
    JMenuItem menuItem = new JMenuItem("Help Homepage");
    menuItem.addActionListener(this);
    myMenu.add(menuItem);
    menuItemNames.put("Help Homepage", new Integer(0));
        
    menuItem = new JMenuItem("About VlUM");
    menuItem.addActionListener(this);
    myMenu.add(menuItem);
    menuItemNames.put("About VlUM", new Integer(1));

}
	/**
	 * Invoked when an action occurs.
	 */
public void actionPerformed(java.awt.event.ActionEvent e) {
    Object source = e.getSource();
    String t = null;
    try {
        if (source.getClass() == Class.forName("javax.swing.JMenuItem"))
            t = ((JMenuItem) source).getText();
        else
            t = e.getActionCommand();
    } catch (ClassNotFoundException ex) {
        t = e.getActionCommand();
    }

    if (menuItemNames.get(t) != null) {
        int menuItem = ((Integer) menuItemNames.get(t)).intValue();
        switch (menuItem) {
            case (0) :
                // Help
                try {
                    myContextData.getAppletContext().showDocument(new URL(myContextData.getBaseUrl() + "help.html"), "topicframe");
                    break;
                } catch (Exception ex) {
                    emit("bad url: "+ex.toString());
                }
            case (1) :
                // About
                try {
					myContextData.getAppletContext().showDocument(new URL(myContextData.getBaseUrl() + "about.html"), "topicframe");
                    break;
                } catch (Exception ex) {
                    emit("bad url: "+ex.toString());
                }
        }
    }
}
}
