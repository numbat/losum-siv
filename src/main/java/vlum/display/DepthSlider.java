package vlum.display;

import java.awt.*;


import javax.swing.*;
import javax.swing.event.*;



/**
 * A slider for manipulating the <i>depth</i> value.
 * Creation date: (2/04/2003 12:39:17 PM)
 * @author: Andrew Lum
 */

public class DepthSlider implements ChangeListener {

    // display panels to be supplied by caller
    private JPanel myContentPane;
    private SquidgePane mySquidgePane;

    // display panels created by this class
    private JPanel myControlPane;

    // the depth slider
    private JSlider myDepthSlider;
    private JLabel myDepthLabel;
    
    private int depthLimit = -1;
    
/**
 * DepthSlider constructor comment.
 */
public DepthSlider(JPanel contentPane, SquidgePane squidgePane) {
	
	// set or create display panes
	myContentPane = contentPane;
	mySquidgePane = squidgePane;
	myControlPane = new JPanel();
	
    myControlPane.setLayout(new BoxLayout(myControlPane, BoxLayout.X_AXIS));
    JPanel labelPane = new JPanel();
    
    labelPane.setLayout(new BoxLayout(labelPane, BoxLayout.Y_AXIS));
    labelPane.setMinimumSize(new Dimension(180, 30));
    myContentPane.add(myControlPane);
    
    myDepthSlider = new JSlider(0, 10, 10);
    myDepthSlider.addChangeListener(this);
    myDepthLabel = new JLabel();
    myControlPane.add(myDepthSlider);

    Dimension minSize = new Dimension(5, 20);
    Dimension prefSize = new Dimension(5, 20);
    Dimension maxSize = new Dimension(10, 20);
    
    myControlPane.add(new Box.Filler(minSize, prefSize, maxSize));
    myControlPane.add(labelPane);
    labelPane.add(myDepthLabel);
    myControlPane.add(new Box.Filler(minSize, prefSize, maxSize));
    
    if (depthLimit == -1) {
		depthLimit = 10;
    }

    mySquidgePane.setDepthLimit(depthLimit);
	myDepthSlider.setValue(depthLimit);
    setDepthLabel(depthLimit);
}
private void setDepthLabel(int val) {
    myDepthLabel.setText("Depth Limit is " + val);
}
public void setDepthLimit(int val) {
    mySquidgePane.setDepthLimit(val);
	myDepthSlider.setValue(val);
    setDepthLabel(val);
}
	/**
	 * Invoked when the target of the listener has changed its state.
	 *
	 * @param e  a ChangeEvent object
	 */
public void stateChanged(javax.swing.event.ChangeEvent e) {
    JSlider source = (JSlider) e.getSource();
	if (source == myDepthSlider) {
		mySquidgePane.setDepthLimit((int) source.getValue());
		mySquidgePane.setSelectedTopic(mySquidgePane.getSelectedTopic());
		setDepthLabel((int) source.getValue());
	}	
	
	
}
}
