package vlum.display;

import java.awt.event.*;
import java.util.*;
import java.net.*;

import javax.swing.*;

import vlum.*;

/**
 * The <i>Action</i> menu allows actions to be taken out on a
 * particular selected concept.
 * Creation date: (3/04/2003 3:57:53 PM)
 * @author: Andrew Lum
 */
public class ActionMenu extends GenericMenu implements ActionListener {

JMenuItem backMenuItem;
	
/**
 * ActionMenu constructor comment.
 */
public ActionMenu(SquidgePane squidgePane, ContextData contextData) {
    mySquidgePane = squidgePane;
    myContextData = contextData;
    
	menuItemNames = new Hashtable();
    
	myMenu = new JMenu("Action");
    
    backMenuItem = new JMenuItem("Go Back One");
    backMenuItem.addActionListener(this);
    backMenuItem.setEnabled(false);
    myMenu.add(backMenuItem);
    menuItemNames.put("Go Back One", new Integer(0));

    JMenuItem menuItem = new JMenuItem("View Selected");
    menuItem.addActionListener(this);
    myMenu.add(menuItem);
    menuItemNames.put("View Selected", new Integer(1));

    menuItem = new JMenuItem("Search Titles");
    menuItem.addActionListener(this);
    myMenu.add(menuItem);
    menuItemNames.put("Search Titles", new Integer(2));

    menuItem = new JMenuItem("View Evidence");
    menuItem.addActionListener(this);
    myMenu.add(menuItem);
    menuItemNames.put("View Evidence", new Integer(3));

    myMenu.addSeparator();

    menuItem = new JMenuItem("* Reset *");
    menuItem.addActionListener(this);
    myMenu.add(menuItem);
    menuItemNames.put("* Reset *", new Integer(4));
}
/**
 * Invoked when an action occurs.
 */
public void actionPerformed(java.awt.event.ActionEvent e) {
    Object source = e.getSource();
    String t = null;
    try {
        if (source.getClass() == Class.forName("javax.swing.JMenuItem"))
            t = ((JMenuItem) source).getText();
        else
            t = e.getActionCommand();
    } catch (ClassNotFoundException ex) {
        t = e.getActionCommand();
    }
    if (menuItemNames.get(t) != null) {
        int menuItem = ((Integer) menuItemNames.get(t)).intValue();
        switch (menuItem) {
            case (0) :
                // Go Back One
                if (myContextData.getHistorySize() > 1) {
                    mySquidgePane.setSelectedTopic(myContextData.popHistoryItem());
                }
                if (myContextData.getHistorySize() == 1)
                    disableBack();
                break;
            case (1) :
                // View Selected
                try {
                    String url = mySquidgePane.getSelectedDescription();
                    if (url.startsWith("http://"))
                        myContextData.getAppletContext().showDocument(new URL(url), "topicframe");
                    else
                        myContextData.getAppletContext().showDocument(
                            new URL("fetch.cgi?name=" + url),
                            "topicframe");
                } catch (Exception ex) {
                    //emit("bad url: "+ex.toString());
                }
                break;
            case (2) :
                // Search Titles
                String regex = JOptionPane.showInputDialog(mySquidgePane, "Search String?");
                mySquidgePane.searchFor(regex);
                break;
            case (3) :
                // View Evidence
                try {
                    myContextData.getAppletContext().showDocument(
                        new URL("evidence.html"),
                        "topicframe");
                } catch (Exception ex) {
                    emit("bad url: " + ex.toString());
                }
                break;
            case (4) :
                // Reset
                if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null, "Are you sure you want to reset?", "reset check", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)) {
                    //1resetSquidge();
                }
                break;
        }
    }

}
/**
 * Disable the back button.
 * Creation date: (16/04/2003 11:40:12 AM)
 */
public void disableBack() {
	backMenuItem.setEnabled(false);
	}
/**
 * Enable the back button.
 * Creation date: (16/04/2003 11:40:12 AM)
 */
public void enableBack() {
	backMenuItem.setEnabled(true);
	}
}
