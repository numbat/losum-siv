package vlum.display;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;



import vlum.ContextData;
import vlum.graph.*;

/**
 * Default Select Pane contains buttons for expand, back, select,
 * highlight, infer
 * Creation date: (2/04/2003 12:39:17 PM)
 * @author: Andrew Lum
 */
public class DefaultSelectPane implements ActionListener, ChangeListener {

	private ContextData myContextData;

	// some graph fields to be supplied by caller
	private GraphSetup myGraphSetup;	
	private int myCurrentDisplayIndex;
	
	// display panels to be supplied by caller
    private JPanel myContentPane;
	private SquidgePane mySquidgePane;

	// display panels created by this class
    private JPanel myControlPane;
	 
    // the standard slider
    private JSlider myMarkSepSlider;
    private JLabel myMarkSepLabel;

    // display values
    private int myMarkSepValue;

/**
 * StandardSlider constructor comment.
 */
public DefaultSelectPane(GraphSetup graphSetup, int currentDisplayIndex,JPanel contentPane, SquidgePane squidgePane, ContextData contextData) {
	// store graph fields
	myGraphSetup = graphSetup;
	myCurrentDisplayIndex = currentDisplayIndex;
	myContextData = contextData;
	
	// set or create display panes
	myContentPane = contentPane;
	mySquidgePane = squidgePane;
	myControlPane = new JPanel();

	JButton mySearchButton = new JButton("Search");
	mySearchButton.setActionCommand("search");
	mySearchButton.addActionListener(this);
	mySearchButton.setBackground(new Color(236, 233, 216));	//windows beige
	
	JButton myBackButton = new JButton("Back");
	myBackButton.setActionCommand("back");
	myBackButton.addActionListener(this);
	myBackButton.setBackground(new Color(236, 233, 216));	//windows beige

	JButton mySelectButton = new JButton("Select/Deselect");
	mySelectButton.setActionCommand("select");
	mySelectButton.addActionListener(this);
	mySelectButton.setBackground(new Color(236, 233, 216));	//windows beige
	
	JButton myExpandButton = new JButton("Expand");
	myExpandButton.setActionCommand("expand");
	myExpandButton.addActionListener(this);
	myExpandButton.setBackground(new Color(236, 233, 216));	//windows beige

	JButton myHlightButton = new JButton("Highlight");
	myHlightButton.setActionCommand("hlight");
	myHlightButton.addActionListener(this);
	myHlightButton.setBackground(new Color(236, 233, 216));	//windows beige

	JButton myInferButton = new JButton("Infer");
	myInferButton.setActionCommand("infer");
	myInferButton.addActionListener(this);
	myInferButton.setBackground(new Color(236, 233, 216));	//windows beige

	JButton myBlankButton = new JButton("Blank");
	myBlankButton.setActionCommand("blank");
	myBlankButton.addActionListener(this);
	myBlankButton.setBackground(new Color(236, 233, 216));	//windows beige

	myControlPane.add(mySearchButton);
	//myControlPane.add(myBackButton);
	myControlPane.add(mySelectButton);
	//myControlPane.add(myDeselectButton);
	//myControlPane.add(myExpandButton);
	//myControlPane.add(myHlightButton);
	myControlPane.add(myInferButton);
	//myControlPane.add(myBlankButton);
	
	myControlPane.setBackground(new Color(236, 233, 216));	//windows beige
	myContentPane.setBackground(new Color(236, 233, 216));	//windows beige
	
	myControlPane.setLayout(new BoxLayout(myControlPane, BoxLayout.X_AXIS));
    JPanel labelPane = new JPanel();

    labelPane.setLayout(new BoxLayout(labelPane, BoxLayout.Y_AXIS));
    labelPane.setMinimumSize(new Dimension(180, 30));

    myContentPane.add(myControlPane);
    
    myMarkSepValue = 50;

    myMarkSepSlider = new JSlider(0, 100);
    myMarkSepSlider.addChangeListener(this);
    myMarkSepLabel = new JLabel();
	setSepLabel();
    
    //myControlPane.add(myMarkSepSlider);

    Dimension minSize = new Dimension(5, 20);
    Dimension prefSize = new Dimension(5, 20);
    Dimension maxSize = new Dimension(10, 20);

    labelPane.add(myMarkSepLabel);
    
    myControlPane.add(new Box.Filler(minSize, prefSize, maxSize));

    //myControlPane.add(labelPane);
}
/**
 * Change the slider and label value.
 * Creation date: (2/04/2003 3:08:15 PM)
 */
public void changeValue(int value) {
    myMarkSepValue = 50;
    myMarkSepSlider.setValue(myMarkSepValue);
    setSepLabel();
	
}
/**
 * Set the label to be displayed.
 * Creation date: (2/04/2003 3:08:15 PM)
 */
private void setSepLabel() {
    int val = myMarkSepValue;
    if (myGraphSetup.isComparison(myCurrentDisplayIndex)) {
        myMarkSepLabel.setText("Standard is " + ((val >= 50) ? (((val - 50) * 2) + "% Above Average") : (((50 - val) * 2) + "% Below Average")));
    } else {
        myMarkSepLabel.setText("Standard is " + val + "%");
    }
}
/**
 * Change the visibility of the slider.
 * Creation date: (2/04/2003 3:08:15 PM)
 */
public void setVisible(boolean visible) {
      myMarkSepSlider.setEnabled(visible);
      myMarkSepLabel.setVisible(visible);
	
}

public void actionPerformed(ActionEvent e) {
	if ("back".equals(e.getActionCommand())) {
		if (myContextData.getHistorySize() > 1) {
			mySquidgePane.setSelectedTopic(myContextData.popHistoryItem());
		}
	}
	if ("search".equals(e.getActionCommand())) {	
		String regex = JOptionPane.showInputDialog(mySquidgePane, "Search String?");
		myContextData.addClickstream("SEARCH-" + regex);
    	mySquidgePane.searchFor(regex);
	}
	if ("select".equals(e.getActionCommand())) {
		if (myContextData.getGraphModel().myOverlayHorizontal.isSelected(mySquidgePane.getCurrentSelectedTitle()) == false) {
			// select
			myContextData.addClickstream("SELECT-" + mySquidgePane.getCurrentSelectedTitle());
			myContextData.applyOverlayHorizontalSingle(mySquidgePane.getCurrentSelectedTitle());
			mySquidgePane.setSelectedTopic(mySquidgePane.getSelectedTopic());
		} else {
			// deselect
			myContextData.addClickstream("DESELECT-" + mySquidgePane.getCurrentSelectedTitle());
			myContextData.removeOverlayHorizontalSingle(mySquidgePane.getCurrentSelectedTitle());
			mySquidgePane.setSelectedTopic(mySquidgePane.getSelectedTopic());
		}
	}
	if ("expand".equals(e.getActionCommand())) {
		mySquidgePane.setSelectedTopic(myContextData.getSelected());
	}
	if ("hlight".equals(e.getActionCommand())) {
		mySquidgePane.highlightSet();
	}
	if ("infer".equals(e.getActionCommand())) {
		myContextData.addClickstream("INFER-" + mySquidgePane.getCurrentSelectedTitle());
		myContextData.applyOverlayInference(mySquidgePane.getCurrentSelectedTitle(), mySquidgePane.getDepthLimit());
		mySquidgePane.setSelectedTopic(mySquidgePane.getCurrentSelected());
	}
	if ("blank".equals(e.getActionCommand())) {
		if (mySquidgePane.isLoading()) {
			mySquidgePane.setLoading(false);			
		} else {
			mySquidgePane.setLoading(true);
		}
		mySquidgePane.setSelectedTopic(mySquidgePane.getCurrentSelected());
	}
} 

	/**
	 * Invoked when the target of the listener has changed its state.
	 * @param e  a ChangeEvent object
	 */
public void stateChanged(javax.swing.event.ChangeEvent e) {
    JSlider source = (JSlider) e.getSource();
	if (source == myMarkSepSlider) {
    	int val = (int) source.getValue();
    	if (val == myMarkSepValue) {
    	    return;
    	}
    	myMarkSepValue = val;
    	setSepLabel();
    	mySquidgePane.setMarkSeparationValue(val / 100f);
    }
}
}
