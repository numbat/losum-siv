package vlum.display;

import java.awt.event.*;


import javax.swing.*;


/**
 * The <i>Display</i> menu contains items that allow the user
 * to change the look and feel of the display.
 * Creation date: (2/04/2003 8:05:55 PM)
 * @author: Andrew Lum
 */
public class DrawMenu extends GenericMenu implements ItemListener {


    
    private JCheckBoxMenuItem dragableCheckBox = null;
    private JCheckBoxMenuItem antiAliasCheckBox = null;
    private JCheckBoxMenuItem drawAlphaCheckBox = null;
    private JCheckBoxMenuItem animateCheckBox = null;
/**
 * DisplayMenu constructor comment.
 */
public DrawMenu(SquidgePane squidgePane) {
	mySquidgePane = squidgePane;

    // setup the draw menu
    myMenu = new JMenu("Draw");
    
    dragableCheckBox = new JCheckBoxMenuItem("Drag");
    dragableCheckBox.addItemListener(this);
    myMenu.add(dragableCheckBox);
        
    antiAliasCheckBox = new JCheckBoxMenuItem("AntiAlias");
    antiAliasCheckBox.addItemListener(this);
    myMenu.add(antiAliasCheckBox);
     
    drawAlphaCheckBox = new JCheckBoxMenuItem("Alpha");
    drawAlphaCheckBox.addItemListener(this);
    myMenu.add(drawAlphaCheckBox);
        
    animateCheckBox = new JCheckBoxMenuItem("Animate");
    animateCheckBox.addItemListener(this);
    myMenu.add(animateCheckBox);
}
	/**
	 * Invoked when an item has been selected or deselected.
	 * The code written for this method performs the operations
	 * that need to occur when an item is selected (or deselected).
	 */
public void itemStateChanged(java.awt.event.ItemEvent e) {
    if (e.getItemSelectable() == dragableCheckBox)
        mySquidgePane.setDragable(e.getStateChange() == ItemEvent.SELECTED);
    if (e.getItemSelectable() == antiAliasCheckBox)
        mySquidgePane.setAntiAlias(e.getStateChange() == ItemEvent.SELECTED);
    if (e.getItemSelectable() == drawAlphaCheckBox)
        mySquidgePane.setAlphaTransparency(e.getStateChange() == ItemEvent.SELECTED);
    if (e.getItemSelectable() == animateCheckBox)
        mySquidgePane.setAnimate(e.getStateChange() == ItemEvent.SELECTED);	
}
}
