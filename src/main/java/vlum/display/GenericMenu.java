package vlum.display;


import java.util.*;


import javax.swing.*;


import vlum.*;


/**
 * Generic menu for VlUM.
 * Creation date: (3/04/2003 12:30:12 PM)
 * @author: Andrew Lum
 */
public abstract class GenericMenu {
	protected ContextData myContextData;
		
	protected SquidgePane mySquidgePane;
    protected JMenu myMenu;

    protected Hashtable menuItemNames;
  protected static void emit (String s) {
    System.out.println(s);
  }
	/**
 * Get the menu
 * Creation date: (3/04/2003 11:46:48 AM)
 * @return javax.swing.JMenu
 */
public JMenu getMenu() {
	return myMenu;
}
}
