/*****************************************************************************
 * Source code information
 * -----------------------
 * Original author    Andrew Lum, University of Sydney
 * Author email       alum@it.usyd.edu.au
 * Package            Andrew
 * Web                
 * Created            9 August 2003
 * Filename           $RCSfile: FrontEnd.java,v $
 * Revision           $Revision: 1.3 $
 * Release status     $State: Exp $
 *
 * Last modified on   $Date: 2003/10/03 05:13:32 $
 *               by   $Author: alum $
 *
 * ****************************************************************************/
package vlum.web;

import vlum.*;
import vlum.display.*;

/**
 * A web front end for interacting with VlUM via
 * client side script calls.
 * Creation date: (29/04/2003 2:56:15 PM)
 *
 * @author: Andrew Lum
 */

public class FrontEnd {
    private SquidgePane mySquidgePane;
    private ContextData myContextData;

    /**
     * FrontEnd constructor comment.
     */
    public FrontEnd(ContextData data, SquidgePane squidge) {
        myContextData = data;
        mySquidgePane = squidge;
    }

    /**
     * Search for titles.
     * Creation date: (4/05/2003 2:03:23 PM)
     */
    public void doSearch(String regex) {
        mySquidgePane.searchFor(regex);
    }

    /**
     * Get the current element's title.
     * Creation date: (29/04/2003 3:03:32 PM)
     */
    public String getCurrentTitle() {
        return mySquidgePane.getCurrentSelectedTitle();
    }

    /**
     * Get the click stream
     * Creation date: (29/04/2003 3:03:32 PM)
     */
    public String getClickStream() {
        return myContextData.getClickstream();
    }

    /**
     * Add to the click stream
     * Creation date: (29/04/2003 3:03:32 PM)
     */
    public void addClickStream(String search) {
        myContextData.addClickstream("SEARCH[" + search + "]");
    }

    /**
     * Reset the click stream
     * Creation date: (29/04/2003 3:03:32 PM)
     */
    public void resetClickStream() {
        myContextData.resetClickstream();
    }

    /**
     * Go back one.
     * Creation date: (4/05/2003 2:46:01 PM)
     */
    public void goBackOne() {
        if (myContextData.getHistorySize() > 1) {
            mySquidgePane.setSelectedTopic(myContextData.popHistoryItem());
        }
    }

    /**
     * Set the current element's title.
     * Creation date: (29/04/2003 3:03:32 PM)
     */
    public void setCurrentTitle(String s) {
        int cm = -1;
        try {
            cm = myContextData.getGraphModel().getTitleIndex(s);
        } catch (Exception ex) {
            return;
        }
        if (cm != -1) {
            mySquidgePane.setSelectedTopic(cm);
        }
    }
}
