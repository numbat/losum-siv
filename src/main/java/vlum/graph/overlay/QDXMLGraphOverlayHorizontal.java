/*
 * Created on 12/12/2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package vlum.graph.overlay;

import qdxml.DocHandler;
import qdxml.QDParser;

import vlum.graph.GraphModel;
import vlum.graph.model.OWLQDXMLModel;

import java.io.StringReader;
import java.util.*;

/**
 * @author alum
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class QDXMLGraphOverlayHorizontal extends GraphOverlayHorizontal implements DocHandler{
	
	public QDXMLGraphOverlayHorizontal(GraphModel myGraph) {
		super(myGraph); 
	}
	
	/**
	 * Parse the overlay XML file and overlay the ontology.
	 * @param uri the location of the overlay
	 */
	public void parse(String uri) {
        try {
            String data = OWLQDXMLModel.fetch(uri);
            StringReader reader = new StringReader(data);
            QDParser.parse(this, reader);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	public void changeSingle(String title) {
		if (myGraphModel.getTitleIndex(title) != -1) {
			selectedTitles.put(title, new Float(myGraphModel.getReliability(myGraphModel.getTitleIndex(title), 0)));
			myGraphModel.setReliability(title, 0, new Float(1.0).floatValue());
			myGraphModel.setNormalisedReliability(title, 0, new Float(1.0).floatValue());
			changedTitles.addElement(title);
		}
	}

	public void revertSingle(String title) {
		if (selectedTitles.containsKey(title)) {
			myGraphModel.setReliability(title, 0, ((Float)(selectedTitles.get(title))).floatValue());
			myGraphModel.setNormalisedReliability(title, 0, ((Float)(selectedTitles.get(title))).floatValue());
			selectedTitles.remove(title);
		}	
	}

	/**
	 * Reset the score and confidence data to zero.
	 */
	public void clear() {
		for (int i = 0; i < changedTitles.size(); i++) {
			myGraphModel.setReliability((String)changedTitles.elementAt(i), 0, 0f);
			myGraphModel.setNormalisedReliability((String)changedTitles.elementAt(i), 0, 0f);
		}
		changedTitles = new Vector();
	}
	
	public int[] getSelected() {
		Object[] myKeys = selectedTitles.keySet().toArray();
		ArrayList mySelected = new ArrayList();
		int mySelectedTitles[] = new int[myKeys.length];
		
		for (int i = 0; i < myKeys.length; i++) {
			mySelected.add(new Integer(myGraphModel.getTitleIndex((String)myKeys[i])));
			mySelectedTitles[i] = myGraphModel.getTitleIndex((String)myKeys[i]);
		}
		
		return mySelectedTitles;
	}

	public boolean isSelected(String title) {
		if (selectedTitles.containsKey(title)) {
			return true;
		}
		return false;
	}


    /* (non-Javadoc)
     * @see qdxml.DocHandler#startElement(java.lang.String, java.util.Hashtable)
     */
    public void startElement(String tag, Hashtable h) throws Exception { 
        if (tag.equals("result")) {
            String title = (String)h.get("concept");
            String value = (String)h.get("value");
			if (myGraphModel.getTitleIndex(title) != -1) {
				myGraphModel.setReliability(title, 0, new Float(value).floatValue());
				myGraphModel.setNormalisedReliability(title, 0, new Float(value).floatValue());
				changedTitles.addElement(title);
			}
        }
    }

    /* (non-Javadoc)
     * @see qdxml.DocHandler#endElement(java.lang.String)
     */
    public void endElement(String tag) throws Exception {
    }

    /* (non-Javadoc)
     * @see qdxml.DocHandler#startDocument()
     */
    public void startDocument() throws Exception {
    }

    /* (non-Javadoc)
     * @see qdxml.DocHandler#endDocument()
     */
    public void endDocument() throws Exception {
    }

    /* (non-Javadoc)
     * @see qdxml.DocHandler#text(java.lang.String)
     */
    public void text(String str) throws Exception {
    }

    /***************************************************************************
     * Main Function for testing
     *  
     **************************************************************************/
    public static void main(String[] args) {
        try {
            OWLQDXMLModel myModel = new OWLQDXMLModel(null, "http://pc0-g61-2/ontologies/usability.owl", null);
            QDXMLGraphOverlayHorizontal myOverlay = new QDXMLGraphOverlayHorizontal(myModel);
            myOverlay.parse("http://pc0-g61-2/personis/results.cgi?login=alum");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}