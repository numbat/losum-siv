/*
 * Created on 24/09/2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package vlum.graph.overlay;

import vlum.graph.GraphModel;
import vlum.graph.inference.*;
import java.util.*;

/**
 * @author alum
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GraphOverlayInference extends GraphOverlayColour {

	// Inherited fields
	// protected String overlayURI;
	// protected GraphModel myGraphModel;
	// protected Vector changedTitles;

	// back to regular programming

	private Hashtable changedMarks;
	private Hashtable changedNormalisedMarks;
	private Hashtable changedReliability;
	private Hashtable changedNormalisedReliability;
	private Hashtable inferenceDepths;
	private Hashtable inferenceDeltas;

	Inferencer inferenceAverage;

	/**
	 * @param myGraph
	 */
	public GraphOverlayInference(GraphModel myGraph) {
		super(myGraph);
		inferenceAverage = new Averager(myGraph);
		changedMarks = new Hashtable();
		changedNormalisedMarks = new Hashtable();
		inferenceDepths = new Hashtable();
		inferenceDeltas = new Hashtable();
	}
	
	public void doInference(String title, int depth) {
		if (changedMarks.containsKey(title)) {
			clearMark(title);
		}
		saveMark(title);
		float origvalue = myGraphModel.getMark(myGraphModel.getTitleIndex(title), 0);
		float value = inferenceAverage.doInference(title, depth);
		myGraphModel.setMark(title, 0, value);
		myGraphModel.setNormalisedMark(title, 0, value);
		print("Inferred value for " + title + ": +" + (value - origvalue));
		print("Inferred value for " + title + ": " + value);
		inferenceDeltas.put(title, new Float(value - origvalue));
		inferenceDepths.put(title, new Integer(depth));
	}
	
	public void undoInference(String title) {
		clearMark(title);
	}
	
	public float getInferenceDelta(String title) {
		if (inferenceDeltas.containsKey(title)) {	
			return ((Float)inferenceDeltas.get(title)).floatValue();
		} else {
			return -1f;
		}
	}
	
	public int getInferenceDepth(String title) {
		if (inferenceDeltas.containsKey(title)) {	
			return ((Integer)inferenceDepths.get(title)).intValue();
		} else {
			return -1;
		}
	}
	
	private void saveMark(String title) {
		changedMarks.put(title, new Float(myGraphModel.getMark(myGraphModel.getTitleIndex(title), 0)));		
		changedNormalisedMarks.put(title, new Float(myGraphModel.getNormalisedMark(myGraphModel.getTitleIndex(title), 0)));		
	}
	
	private void clearMark(String title) {
		if (changedMarks.containsKey(title)) {
			Float mark = (Float)changedMarks.remove(title);
			myGraphModel.setMark(title, 0, mark.floatValue());
			Float normalisedMark = (Float)changedNormalisedMarks.remove(title);
			myGraphModel.setNormalisedMark(title, 0, mark.floatValue());
			inferenceDeltas.remove(title);
			inferenceDepths.remove(title);
		}
	}
	
	private static void print(String myString) {
		System.out.println(myString);	
	}

}
