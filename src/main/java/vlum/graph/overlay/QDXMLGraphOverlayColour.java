package vlum.graph.overlay;

import qdxml.*;
import vlum.graph.GraphModel;
import vlum.graph.model.OWLQDXMLModel;
//import vlum.graph.model.QDXMLGraphSetup;

import java.io.StringReader;
import java.util.*;

/**
 * @author alum
 * 
 */
public class QDXMLGraphOverlayColour extends GraphOverlayColour implements DocHandler {

    public QDXMLGraphOverlayColour(GraphModel myGraph) {
        super(myGraph);
    }

    /**
     * Parse the overlay XML file and overlay the ontology.
     * 
     * @param uri the location of the overlay
     */
    public void parse(String uri) {
        try {
            String data = OWLQDXMLModel.fetch(uri);
            StringReader reader = new StringReader(data);
            QDParser.parse(this, reader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isChanged(String title) {
        for (int i = 0; i < changedTitles.size(); i++) {
            String s = (String) changedTitles.elementAt(i);
            if (s == title) {
                return true;
            }
        }
        return false;
    }

    /**
     * Reset the score and confidence data to zero.
     */
    public void clear() {
        for (int i = 0; i < changedTitles.size(); i++) {
            myGraphModel.setMark((String) changedTitles.elementAt(i), 0, 0f);
            myGraphModel.setNormalisedMark((String) changedTitles.elementAt(i), 0, 0f);
        }
        changedTitles = new Vector();
        overlayURI = "";
    }

    /**
     * @return
     */
    public String getOverlayURI() {
        return overlayURI;
    }

    /*
     * (non-Javadoc)
     * @see qdxml.DocHandler#startElement(java.lang.String, java.util.Hashtable)
     */
    public void startElement(String tag, Hashtable h) throws Exception {
        if (tag.equals("result")) {
            String title = (String)h.get("concept");
            String value = (String)h.get("value");
            if (myGraphModel.getTitleIndex(title) != -1) {
                myGraphModel.setMark(title, 0, new Float(value).floatValue());
                myGraphModel.setNormalisedMark(title, 0, new Float(value).floatValue());
                changedTitles.addElement(title);
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see qdxml.DocHandler#endElement(java.lang.String)
     */
    public void endElement(String tag) throws Exception {
    }

    /*
     * (non-Javadoc)
     * @see qdxml.DocHandler#startDocument()
     */
    public void startDocument() throws Exception {
    }

    /*
     * (non-Javadoc)
     * @see qdxml.DocHandler#endDocument()
     */
    public void endDocument() throws Exception {
    }

    /*
     * (non-Javadoc)
     * @see qdxml.DocHandler#text(java.lang.String)
     */
    public void text(String str) throws Exception {
    }

    /***************************************************************************
     * Main Function for testing
     *  
     **************************************************************************/
    public static void main(String[] args) throws Exception {
        try {
            OWLQDXMLModel myModel = new OWLQDXMLModel(null, "http://pc0-g61-2/ontologies/usability.owl", null);
            QDXMLGraphOverlayColour myOverlay = new QDXMLGraphOverlayColour(myModel);
            myOverlay.parse("http://pc0-g61-2/personis/results.cgi?login=alum");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}