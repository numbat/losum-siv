/*
 * Created on 16/09/2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package vlum.graph.overlay;
import java.util.*;

import vlum.graph.GraphModel;
/**
 * @author alum
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public abstract class GraphOverlay {
	protected String overlayURI;
	protected GraphModel myGraphModel;
	protected Vector changedTitles;

	protected abstract void parse(String uri);

	protected static String toRDFTitle(String key) {
		StringBuffer cur = new StringBuffer();
		//replace ampersands with &amp; and remove quotation marks
		for (int i = 0; i < key.length(); ++i) {
			char c = key.charAt(i);
			switch (c) {
			case '&':
				cur.append("&amp;");
				break;
			case '"':
				break;
			default:
				if (c > 127)
					cur.append("%" + Integer.toHexString((int)c));
				else
					cur.append(c);
			}
		}
		return cur.toString();
	}


}
