/*
 * Created on 12/12/2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package vlum.graph.overlay;

import org.apache.xerces.parsers.*;
import org.w3c.dom.*;

import vlum.graph.GraphModel;

import java.util.*;

/**
 * @author alum
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GraphOverlayHorizontal extends GraphOverlay {
	protected Hashtable selectedTitles;
	
	public GraphOverlayHorizontal(GraphModel myGraph) {
		myGraphModel = myGraph;
		changedTitles = new Vector();
		selectedTitles = new Hashtable();
		return;
	}
	
	/**
	 * Parse the overlay XML file and overlay the ontology.
	 * @param uri the location of the overlay
	 */
	public void parse(String uri) {
		DOMParser parser = new DOMParser();
		try {
			parser.parse(uri);
			Document myDocument = parser.getDocument();
			NodeList resultList = myDocument.getElementsByTagName("result");
			for (int i=0; i < resultList.getLength(); i++) {
				String title = GraphOverlayHorizontal.toRDFTitle(((Element) resultList.item(i)).getAttribute("concept"));
				String value = ((Element) resultList.item(i)).getAttribute("value");
				if (myGraphModel.getTitleIndex(title) != -1) {
					myGraphModel.setReliability(title, 0, new Float(value).floatValue());
					myGraphModel.setNormalisedReliability(title, 0, new Float(value).floatValue());
					changedTitles.addElement(title);
				}
			}
		} catch (Exception e) {
			System.out.println("Horizontal Overlay - not found or unparsable");
			e.printStackTrace();
		}
	}

	public void changeSingle(String title) {
		if (myGraphModel.getTitleIndex(title) != -1) {
			selectedTitles.put(title, new Float(myGraphModel.getReliability(myGraphModel.getTitleIndex(title), 0)));
			myGraphModel.setReliability(title, 0, new Float(1.0).floatValue());
			myGraphModel.setNormalisedReliability(title, 0, new Float(1.0).floatValue());
			changedTitles.addElement(title);
		}
	}

	public void revertSingle(String title) {
		if (selectedTitles.containsKey(title)) {
			myGraphModel.setReliability(title, 0, ((Float)(selectedTitles.get(title))).floatValue());
			myGraphModel.setNormalisedReliability(title, 0, ((Float)(selectedTitles.get(title))).floatValue());
			selectedTitles.remove(title);
		}	
	}

	/**
	 * Reset the score and confidence data to zero.
	 */
	public void clear() {
		for (int i = 0; i < changedTitles.size(); i++) {
			myGraphModel.setReliability((String)changedTitles.elementAt(i), 0, 0f);
			myGraphModel.setNormalisedReliability((String)changedTitles.elementAt(i), 0, 0f);
		}
		changedTitles = new Vector();
	}
	
	public int[] getSelected() {
		Object[] myKeys = selectedTitles.keySet().toArray();
		ArrayList mySelected = new ArrayList();
		int mySelectedTitles[] = new int[myKeys.length];
		
		for (int i = 0; i < myKeys.length; i++) {
			mySelected.add(new Integer(myGraphModel.getTitleIndex((String)myKeys[i])));
			mySelectedTitles[i] = myGraphModel.getTitleIndex((String)myKeys[i]);
		}
		
		return mySelectedTitles;
	}

	public boolean isSelected(String title) {
		if (selectedTitles.containsKey(title)) {
			return true;
		}
		return false;
	}

	public static void main(String argv[]) {
		System.out.println("Hello!");
	}
	
}