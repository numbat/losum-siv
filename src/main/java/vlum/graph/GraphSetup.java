
package vlum.graph;


public abstract class GraphSetup {

	protected String[] displayIds;
	protected String[] relationshipIds;

	final String MECUREO_NS =
		"http://www.it.usyd.edu.au/~alum/ontologies/mecureo#";

	public abstract int displaysLength();
	public abstract int getComparisonBase(int index);
	public abstract int getComparisonSubtractor(int index);
	
	public abstract String getDisplayType(int index);
	public abstract int getId(String s);
	public abstract String getNodeName(int index);	
	public abstract String getNodeReliabilityTag(int index);
	public abstract String getNodeTag(int index);
	public abstract String getNodeText(int index, String s);

	public abstract String getNodeToolTip(int index);

	public abstract String getRelationshipType(int index);
	
	public abstract int getViewNameIndex(String s);
	
	public abstract int getViewTagIndex(String s);
	
	public abstract boolean isAdjustable(int index);
	
	public abstract boolean isComparison(int index);
	
	public abstract boolean isViewable(int index);
	public abstract int relationshipsLength();
	
}