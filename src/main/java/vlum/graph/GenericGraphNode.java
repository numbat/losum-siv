/*****************************************************************************
 * Source code information
 * -----------------------
 * Original author    Andrew Lum, University of Sydney
 * Author email       alum@it.usyd.edu.au
 * Package            Andrew
 * Web                
 * Created            9 August 2003
 * Filename           $RCSfile: GenericGraphNode.java,v $
 * Revision           $Revision: 1.3 $
 * Release status     $State: Exp $
 *
 * Last modified on   $Date: 2003/09/08 23:53:42 $
 *               by   $Author: alum $
 *
 * ****************************************************************************/
package vlum.graph;

import java.util.*;

/**
 * This class defines a generic node with descriptive links
 * for a directed graph. The node holds additional information
 * on score and reliability.
 *
 * Creation date: (18/06/2002 10:38:34 AM)
 * @author: Andrew Lum
 */
public class GenericGraphNode {

	/* Node attributes */
	private String key;
	private String name = "";				// the preferred label of this concept
	private String description = "";		// this is (supposedly) a uri describing this concept
	private String type = "";				// the type of node
	private String definition = "";			// the definition for this concept
	private String weight = "";				// the weight for this concept as per Mecureo
	
	private int views;				// the number of views 
	private float[] score;
	private float[] certainty;

	private float[] normalisedScore;
	private float[] normalisedCertainty;

	private Object[] peers;

	/**
	 * GenericGraphNode constructor comment.
	 */
	public GenericGraphNode() {
	    this(1);
	}
	
	/**
	 * GenericGraphNode constructor comment.
	 */
	public GenericGraphNode(int v) {
	    views = v;

	    score = new float[views];
		certainty = new float[views];
		normalisedScore = new float[views];
		normalisedCertainty = new float[views];

		setDefaultValues(score);
		setDefaultValues(certainty);
		setDefaultValues(normalisedScore);
		setDefaultValues(normalisedCertainty);
	}
	
	/**
	 * Set the default values for the score and certainty arrays.
	 */	
	public void setDefaultValues(float[] valueArray) {
	    for (int i=0; i < valueArray.length; i++ ) {
	        valueArray[i] = 0f;
	    }
	}
	
	public void print() {
	    System.out.println("Name: " + name);
	    System.out.println("\tDescription: " + description);
	    System.out.println("\tNode Type: " + type);
	    for (int i=0; i < peers.length; i++) {
	        GenericGraphPeer currPeer = (GenericGraphPeer)peers[i];
		    System.out.println("\tPeer: " + currPeer.getDescription() + " " + currPeer.getEndNode());
	    }
	}
	
	/**
	 * Get certainty.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @return float
	 */
	public float getCertainty(int i) {
		return certainty[i];
	}
	/**
	 * Get description.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @return java.lang.String
	 */
	public java.lang.String getDescription() {
		return description;
	}
	/**
	 * Get key.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @return java.lang.String
	 */
	public java.lang.String getKey() {
		return key;
	}
	/**
	 * Get name.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @return java.lang.String
	 */
	public java.lang.String getName() {
		return name;
	}
	/**
	 * Insert the method's description here.
	 * Creation date: (21/06/2002 2:21:20 PM)
	 * @return float[]
	 */
	public float getNormalisedCertainty(int i) {
		return normalisedCertainty[i];
	}
	/**
	 * Insert the method's description here.
	 * Creation date: (21/06/2002 2:21:20 PM)
	 * @return float[]
	 */
	public float getNormalisedScore(int i) {
		return normalisedScore[i];
	}
	/**
	 * Get the number of peers.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @return java.util.Vector
	 */
	public int getPeerCount() {
		return peers.length;
	}
	/**
	 * Get peers.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @return java.util.Vector
	 */
	public Vector getPeers() {
		Vector rpeers = new Vector();
		for (int i = 0; i < peers.length; i++) {
			rpeers.addElement(peers[i]);
		}
		return rpeers;
	}
	
	/**
	 * Get peers.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @return java.util.Vector
	 */
	public Vector getPeers(String type) {
		Vector myPeers = new Vector();
		if (type != null) {
			for (int i = 0; i < peers.length; i++) {
				GenericGraphPeer myPeer = (GenericGraphPeer) peers[i];
				if (myPeer.getDescription().equals(type)) {
					myPeers.addElement(myPeer);
				}
			}
		} else {
			myPeers = getPeers();
		}
		return myPeers;
	}
	
	/**
	 * Get score.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @return int
	 */
	public float getScore(int i) {
		return score[i];
	}
	/**
	 * Insert the method's description here.
	 * Creation date: (12/10/2002 12:25:35 AM)
	 * @return java.lang.String
	 */
	public java.lang.String getType() {
		return type;
	}
	/**
	 * Set certainty.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @param newCertainty float
	 */
	public void setCertainty(int i, float c) {
		certainty[i] = c;
	}
	/**
	 * Set description.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @param newDescription java.lang.String
	 */
	public void setDescription(java.lang.String newDescription) {
		description = newDescription;
	}
	/**
	 * Set key.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @param newKey java.lang.String
	 */
	public void setKey(java.lang.String newKey) {
		key = newKey;
	}
	/**
	 * Set name.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @param newName java.lang.String
	 */
	public void setName(java.lang.String newName) {
		name = newName;
	}
	/**
	 * Insert the method's description here.
	 * Creation date: (21/06/2002 2:21:20 PM)
	 * @param newNormalisedCertainty float[]
	 */
	public void setNormalisedCertainty(int i, float c) {
		normalisedCertainty[i] = c;
	}
	/**
	 * Insert the method's description here.
	 * Creation date: (21/06/2002 2:21:20 PM)
	 * @param newNormalisedScore float[]
	 */
	public void setNormalisedScore(int i, float s) {
		normalisedScore[i] = s;
	}
	/**
	 * Set score.
	 * Creation date: (19/06/2002 10:27:31 AM)
	 * @param i int		index of the score in the views
	 * @param s float	the score
	 */
	public void setScore(int i, float s) {
		score[i] = s;
	}
	/**
	 * Insert the method's description here.
	 * Creation date: (12/10/2002 12:25:35 AM)
	 * @param newType java.lang.String
	 */
	public void setType(java.lang.String newType) {
		type = newType;
	}

	/**
	 * Set the peers
	 */
	public void setPeers(Object[] myPeers) {
		peers = myPeers;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}
}

