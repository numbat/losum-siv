package vlum.graph.model;

import qdxml.DocHandler;
import qdxml.QDParser;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import java.awt.Component;
import java.io.*;

import java.net.*;

import vlum.graph.*;

import java.lang.reflect.*;

/**
 * This class is the most basic possible implementation of the DocHandler class.
 * It simply reports all events to System.out as they occur.
 */
public class OWLQDXMLModel extends GraphModel implements DocHandler {

//    private static final int maxNodeCount = 2048;
    private int indent = 0;
    private boolean isTitle = false;
    private GenericGraphNode currentNode;
    private ArrayList tempNodes;
    private ArrayList currentPeers;
    private Method currentSetterMethod;
    
    public OWLQDXMLModel() {
        super();
    }

    public OWLQDXMLModel(GraphSetup s, String uri) throws Exception {
        this(s, uri, null);
    }

    public OWLQDXMLModel(GraphSetup s, String uri, Component parentFrame) throws Exception {
        timestamp("start");
        memorystamp("start");
        
        // initialise object graph attributes
        nameMap = new Hashtable();
        aboutMap = new Hashtable();
        modelSize = 0;
        markCount = 1;

        // initialise parsing attributes
        tempNodes = new ArrayList();
        currentPeers = new ArrayList();
        currentNode = new GenericGraphNode();

        // run the QDXML Parser to build nameMap, aboutMap, and tempNodes
        timestamp("start fetch file");
        String data = fetch(uri);
        StringReader reader = new StringReader(data);
        timestamp("start parsing");
        QDParser.parse(this, reader);

        // transfer nodes from ArrayList tempNodes[] to Array nodes[]
        timestamp("start transfer nodes");
        Object[] myNodes = tempNodes.toArray();
        nodes = new GenericGraphNode[myNodes.length];        
        for (int i=0; i< myNodes.length; i++) {
            currentNode = (GenericGraphNode) myNodes[i];
            nodes[i] = currentNode;
        }
        
		// now weave the peers
        timestamp("weave peers");
		peers = new int[nodes.length][];
		for (int i = 0; i < nodes.length; i++) {
			Vector p = nodes[i].getPeers();
			peers[i] = new int[p.size()];
			for (int j = 0; j < p.size(); j++) {
				GenericGraphPeer myPeer = (GenericGraphPeer) nodes[i].getPeers().elementAt(j);
				peers[i][j] = ((Integer) aboutMap.get(myPeer.getEndNode())).intValue();
			}
		}
        timestamp("end");
        memorystamp("end");
    }

    // implementation of DocHandler is above this line

    public static String fetch(String myUrl) throws Exception {
        URL url = new URL(myUrl);
        // try opening the URL
        URLConnection urlConnection = url.openConnection();
        urlConnection.setAllowUserInteraction(false);
        InputStream urlStream = url.openStream();
        //String type = URLConnection.guessContentTypeFromStream(urlStream);

        // search the input stream for links
        // first, read in the entire URL
        byte b[] = new byte[100000];
        int numRead = urlStream.read(b);
        String content = new String(b, 0, numRead);
        while (numRead != -1) {
            numRead = urlStream.read(b);
            if (numRead != -1) {
                String newContent = new String(b, 0, numRead);
                content += newContent;
            }
        }
        urlStream.close();
        return (content);
    }

    /***************************************************************************
     * Implementation of DocHandler is below this line
     *  
     **************************************************************************/
    public void startDocument() {
        //System.out.println(" start document");
    }

    public void endDocument() {
        //System.out.println(" end document");
    }

    // process the description for this element
    private void doConcept(Hashtable h) {
        // set the new current concept description
        String desc = "#" + h.get("rdf:ID");
        currentNode.setDescription(desc);
    }

    // process the title for this element
    private void doTitle(String text) {
        currentNode.setName(text);
    }
    
    // process the a single peer for this element
    private void doPeer(String elem, Hashtable h) {
        String resource = (String) h.get("rdf:resource");
        GenericGraphPeer myPeer = new GenericGraphPeer(currentNode.getDescription(), resource, elem);
        currentPeers.add(myPeer);
    }
    
    // start tag for an element
    public void startElement(String elem, Hashtable h) {
        if (elem.equals("Concept")) {
            doConcept(h);
        }
        if (elem.equals("dc:Title")) {
            isTitle = true;
        }
        else if (elem.substring(0,3).equals("has")){
            doPeer(elem, h);
        }
    }

    // end tag for an element
    public void endElement(String elem) {
        indent = indent - 4;
        if (elem.equals("Concept")) {
            // first save the current object to tempNodes
            currentNode.setType("No Type");
            Object[] myArray = currentPeers.toArray();
        	currentNode.setPeers(myArray);
        	tempNodes.add(currentNode);
        	nameMap.put(currentNode.getName(), new Integer(modelSize));
        	aboutMap.put(currentNode.getDescription(), new Integer(modelSize));
        	// now reset the current objects
        	currentNode = new GenericGraphNode();
        	currentPeers = new ArrayList();
        	modelSize++;
        }
    }

    // text inside xml tags
    public void text(String text) {
        if (isEmpty(text)) {
            return;
        }
        if (isTitle) {
            doTitle(text);
            isTitle = false;
        }
    }

    // white space checker
    public static boolean isEmpty(String s) {
        if (s == null)
            return true;
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isWhitespace(s.charAt(i)))
                return false;
        }
        return true;
    }

}