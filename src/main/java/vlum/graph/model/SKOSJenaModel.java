/*****************************************************************************
 * Source code information
 * -----------------------
 * Original author    Andrew Lum, University of Sydney
 * Author email       alum@it.usyd.edu.au
 * Package            SIV
 * Web                
 * Created            9 August 2003
 * Filename           $RCSfile: OWLGraphModel.java,v $
 * Revision           $Revision: 1.14 $
 * Release status     $State: Exp $
 *
 * Last modified on   $Date: 2003/10/03 05:13:31 $
 *               by   $Author: alum $
 *
 * ****************************************************************************/

package vlum.graph.model;

import java.io.InputStream;
import java.util.*;
import java.awt.*;

import javax.swing.ProgressMonitor;

import vlum.graph.GenericGraphNode;
import vlum.graph.GenericGraphPeer;
import vlum.graph.GraphModel;
import vlum.graph.GraphSetup;

import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.graph.*;

import mecureo.output.parser.*;

/**
 * Object based implementation of the GraphModel class.
 * Creation date: (18/06/2002 9:53:04 AM)
 *
 * @author: Andrew Lum
 */
public class SKOSJenaModel extends GraphModel {

	  protected static final String uri ="http://www.w3.org/2004/02/skos/core#";

	  /** returns the URI for this schema
	   * @return the URI for this schema
	   */
	  public static String getURI() {
	        return uri;
	  }

	  private static Model m = ModelFactory.createDefaultModel();

	  public static final Resource Concept = m.createResource(uri + "Concept" );
	  public static final Resource ConceptScheme = m.createResource(uri + "ConceptScheme" );
	  public static final Resource CollectableProperty = m.createResource(uri + "CollectableProperty" );
	  public static final Resource Collection = m.createResource(uri + "Collection" );
	  public static final Resource OrderedCollection = m.createResource(uri + "OrderedCollection" );
	  
	  public static final Property altLabel = m.createProperty(uri, "altLabel" );
	  public static final Property altSymbol = m.createProperty(uri, "altSymbol" );
	  public static final Property broader = m.createProperty(uri, "broader" );
	  public static final Property changeNote = m.createProperty(uri, "changeNote" );
	  public static final Property definition = m.createProperty(uri, "definition" );
	  public static final Property editorialNote = m.createProperty(uri, "editorialNote" );
	  public static final Property example = m.createProperty(uri, "example" );
	  public static final Property hasTopConcept = m.createProperty(uri, "hasTopConcept" );
	  public static final Property hiddenLabel = m.createProperty(uri, "hiddenLabel" );
	  public static final Property historyNote = m.createProperty(uri, "historyNote" );
	  public static final Property isPrimarySubjectOf = m.createProperty(uri, "isPrimarySubjectOf" );
	  public static final Property isSubjectOf = m.createProperty(uri, "isSubjectOf" );
	  public static final Property member = m.createProperty(uri, "member" );
	  public static final Property memberList = m.createProperty(uri, "memberList" );
	  public static final Property narrower = m.createProperty(uri, "narrower" );
	  public static final Property note = m.createProperty(uri, "note" );
	  public static final Property prefLabel = m.createProperty(uri, "prefLabel" );
	  public static final Property prefSymbol = m.createProperty(uri, "prefSymbol" );
	  public static final Property primarySubject = m.createProperty(uri, "primarySubject" );
	  public static final Property related = m.createProperty(uri, "related" );
	  public static final Property scopeNote = m.createProperty(uri, "scopeNote" );
	  public static final Property semanticRelation = m.createProperty(uri, "semanticRelation" );
	  public static final Property subject = m.createProperty(uri, "subject" );
	  public static final Property subjectIndicator = m.createProperty(uri, "subjectIndicator" );
	  public static final Property symbol = m.createProperty(uri, "symbol" );
	
	  protected static final String uri2 = "http://www.it.usyd.edu.au/~niu/ontology#";
	  
	  public static final Property cweight = m.createProperty(uri2, "cweight" );
	  public static final Property weight = m.createProperty(uri2, "weight" );
	  public static final Property keyword = m.createProperty(uri2, "keyword" );
	  public static final Property type = m.createProperty(uri2, "type" );
	  public static final Property strength = m.createProperty(uri2, "strength" );
	  
	private static final String DC_TITLE = "http://purl.org/dc/elements/1.1/Title";
	//private static final int maxNodeCount = 2048;


	public SKOSJenaModel(GraphSetup s, String uri) throws Exception {
		this(s, uri, null);
	}
	public SKOSJenaModel(GraphSetup s, String uri, Component parentFrame)
		throws Exception {

		int doclen;

		// get ontology model
		try {

			timestamp("start");
			memorystamp("start");
			
			timestamp("start jena fetch and parse");	
			
			Model model = ModelFactory.createDefaultModel();
	        model.read(uri);
			
			markCount = s.displaysLength(); // number of views for this graph

			Iterator it = model.listSubjectsWithProperty(prefLabel);
			Vector buffer = new Vector();
			
			timestamp("start building nodes");
			while (it.hasNext()) {
				Resource i = (Resource) it.next();
				buffer.add(i);
			}

			doclen = buffer.size();
			
			nameMap = new Hashtable();
			aboutMap = new Hashtable();
			nodes = new GenericGraphNode[doclen]; // initialise node list

			modelSize = 0;

			for (int i = 0; i < doclen; i++) {
				GenericGraphNode myNode = new GenericGraphNode(markCount);
				nodes[i] = myNode;
				modelSize++;
				
				// temporary storage for peers
				ArrayList myPeers = new ArrayList();
						
				// get name and description
				Resource res = (Resource) buffer.elementAt(i);
				System.out.println("Description: " + res.toString());
				
				// get definition
				StmtIterator iter = model.listStatements(res, definition,(RDFNode)null);
				String def="";
				if (iter.hasNext()) {
					def = iter.nextStatement().getObject().toString();
					System.out.println("\t" + def.toString());
					myNode.setDefinition(def);
				}
				
				// get cweight
				iter = model.listStatements(res, cweight,(RDFNode)null);
				String cweight = "";
				if (iter.hasNext()) {
					cweight = iter.nextStatement().getObject().toString();
					System.out.println("\t" + cweight.toString());
					myNode.setWeight(cweight);
				}
				
				// get broader terms
				iter = model.listStatements(res, broader,(RDFNode)null);
				if (iter.hasNext()) {
					RDFNode broaderConcept = iter.nextStatement().getObject();
					System.out.println("\t" + broaderConcept.toString());
					
					StmtIterator subIter = model.listStatements();
					while (iter.hasNext()) {
						System.out.println("\t\t" + subIter.nextStatement());
					}
					
				}				
				
				/*
				myNode.setDescription(ind.toString());
				it = ind.listProperties();
				while (it.hasNext()) {
					Statement st = (Statement) it.next();
					Triple t = st.asTriple();
					//Node sub = t.getSubject();
					Node pre = t.getPredicate();
					Node obj = t.getObject();
					// if title...
					if (pre.getURI().equals(DC_TITLE)) {
						myNode.setName(obj.toString());
						//System.out.println("\tDisplay Title: " + obj.toString());
					}
					// if peer...
					if (InputOWL.getMecureoProperty(pre.getURI()) != null) {
						String peerType = InputOWL.getMecureoProperty(pre.getURI());
						String resource = obj.toString();
						//myNode.addPeer(resource, peerType);
						GenericGraphPeer myPeer = new GenericGraphPeer(myNode.getDescription(), resource, peerType);
						//System.out.println("\tPeer: " + myNode.getDescription() + "-----" + resource + "-----" + peerType);
						myPeers.add(myPeer);
					}
				}
				// store the peers as an array
				Object[] myArray = myPeers.toArray();
				myNode.setPeers(myArray);
				
				// get the node type
				myNode.setType("No Type");

				// map the name and descriptions in the hash tables
				nameMap.put(myNode.getName(), new Integer(i));
				aboutMap.put(myNode.getDescription(), new Integer(i));
				*/
			}
			
			/*
			
			timestamp("weave peers");
			// now weave the peers
			progressMonitor.setNote("weaving peers");
			progressMonitor.setProgress(0);
			peers = new int[doclen][];
			for (int i = 0; i < doclen; i++) {
				progressMonitor.setProgress(doclen);
				Vector p = nodes[i].getPeers();
				peers[i] = new int[p.size()];
				for (int j = 0; j < p.size(); j++) {
					GenericGraphPeer myPeer = (GenericGraphPeer) nodes[i].getPeers().elementAt(j);
					peers[i][j] = ((Integer) aboutMap.get(myPeer.getEndNode())).intValue();
				}
			}

			*/
			timestamp("end");
			memorystamp("end");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception {
		try {
			SKOSJenaSetup myGraphSetup = new SKOSJenaSetup("http://aefallen/ontologies/atl.skos");
			SKOSJenaModel myGraphModel = new SKOSJenaModel(myGraphSetup, "http://aefallen/ontologies/atl.skos");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
}
