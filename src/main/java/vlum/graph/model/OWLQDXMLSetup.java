
package vlum.graph.model;

import java.io.StringReader;
import java.util.Hashtable;

import qdxml.*;

import qdxml.DocHandler;

import vlum.graph.GraphSetup;

import java.util.*;

public class OWLQDXMLSetup extends GraphSetup implements DocHandler {

	//String[] displayIds;
	//String[] relationshipIds;
    
    ArrayList myRelationships;

	final String MECUREO_NS = "http://www.it.usyd.edu.au/~alum/ontologies/mecureo#";

	public OWLQDXMLSetup(String uri) {
	    myRelationships = new ArrayList();
	    try {
	        String data = OWLQDXMLModel.fetch(uri);
	        StringReader reader = new StringReader(data);
	        QDParser.parse(this, reader);

			// get display
			int c = 1;
			displayIds = new String[c];
			for (int i = 0; i < c; i++) {
				String a = "Default View";
				if (a != null) {
					displayIds[i] = a;
				} else
					displayIds[i] = null;
			}

			Object[] tempArray = myRelationships.toArray();
			
	        relationshipIds = new String[tempArray.length]; 
	        
	        for (int i=0; i< tempArray.length; i++) {
	            String currRel = (String) tempArray[i];
	            relationshipIds[i] = currRel;
	        }
			
		} catch (Exception ex) {
			System.out.println("Graph Setup Initialisation Problem: "
					+ ex.toString());
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * Populate the relationship array with the names of the properties under
	 * the Mecureo namespace field.
	 * 
	 * @param model
	 *            the ontology model that has the properties to extract
	 *  
	 */
	private void addRelationship(String relationship) {
	    myRelationships.add(relationship);
	}

	/**
	 * 
	 * displayLength (current dummy set to return 1)
	 */
	public int displaysLength() {
		return displayIds.length;
	}

	public int getComparisonBase(int index) {
		if (!"comparison".equals(getDisplayType(index)))
			return -1;
		String d = getNodeText(index, "baseData");
		return getId(d);
	}

	public int getComparisonSubtractor(int index) {
		if (!"comparison".equals(getDisplayType(index)))
			return -1;
		String d = getNodeText(index, "compareWith");
		return getId(d);
	}

	public String getDisplayType(int index) {
		//return getAttrText(index, "type");
		return "plain";
	}

	public int getId(String s) {
		for (int i = 0; i < displayIds.length; i++) {
			if (s.equals(displayIds[i]))
				return i;
		}
		return -1;
	}

	public String getNodeName(int index) {
		return getNodeText(index, "name");
	}

	public String getNodeReliabilityTag(int index) {
		return getNodeText(index, "reliabilityTag");
	}

	public String getNodeTag(int index) {
		return getNodeText(index, "tag");
	}

	/* private */
	public String getNodeText(int index, String s) {
		return "";
	}

	public String getNodeToolTip(int index) {
		return getNodeText(index, "tooltip");
	}

	public String getRelationshipType(int index) {
		try {
			return relationshipIds[index];
		} catch (Exception e) {
			return null;
		}
	}

	public int getViewNameIndex(String s) {
		return 0;
	}

	public int getViewTagIndex(String s) {
		return 0;
	}

	public boolean isAdjustable(int index) {
		//String a = getAttrText(index, "adjustable");
		String a = null;
		return (a == null || a.length() == 0 || "true".equals(a));
	}

	public boolean isComparison(int index) {
		//String a = getAttrText(index, "type");
		String a = null;
		return (a != null && a.length() != 0 && "comparison".equals(a));
	}

	public boolean isViewable(int index) {
		//String a = getAttrText(index, "view");
		String a = null;
		return (a == null || a.length() == 0 || "true".equals(a));
	}

	public int relationshipsLength() {
		return relationshipIds.length;
	}

    /* (non-Javadoc)
     * @see qdxml.DocHandler#startElement(java.lang.String, java.util.Hashtable)
     */
    public void startElement(String tag, Hashtable h) throws Exception {
        if (tag.equals("owl:ObjectProperty")) {
            addRelationship((String)h.get("rdf:ID"));
        }
        
    }

    /* (non-Javadoc)
     * @see qdxml.DocHandler#endElement(java.lang.String)
     */
    public void endElement(String tag) throws Exception {
    }

    /* (non-Javadoc)
     * @see qdxml.DocHandler#startDocument()
     */
    public void startDocument() throws Exception {
    }

    /* (non-Javadoc)
     * @see qdxml.DocHandler#endDocument()
     */
    public void endDocument() throws Exception {
    }

    /* (non-Javadoc)
     * @see qdxml.DocHandler#text(java.lang.String)
     */
    public void text(String str) throws Exception {
    }

}