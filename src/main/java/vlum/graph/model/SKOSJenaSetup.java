

package vlum.graph.model;



import vlum.graph.GraphSetup;

/**
 * 
 * @author $Author: alum $
 *
 * This class is a pre-configuration initialiser for the runtime graph model.
 * The relationship and display names are setup at this stage.
 */

public class SKOSJenaSetup extends GraphSetup {
	
	public SKOSJenaSetup(String uri) {
		try {

			// get display
			displayIds = new String[1];
			displayIds[0] = "Default View";

			relationshipIds = new String[3];
			relationshipIds[0] = "broader";
			relationshipIds[1] = "narrower";
			relationshipIds[2] = "related";

		} catch (Exception ex) {
			System.out.println("Graph Setup Initialisation Problem: "
					+ ex.toString());
			ex.printStackTrace();
		}
	}

	public int displaysLength() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getComparisonBase(int index) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getComparisonSubtractor(int index) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getDisplayType(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public int getId(String s) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getNodeName(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getNodeReliabilityTag(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getNodeTag(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getNodeText(int index, String s) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getNodeToolTip(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getRelationshipType(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public int getViewNameIndex(String s) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getViewTagIndex(String s) {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean isAdjustable(int index) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isComparison(int index) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isViewable(int index) {
		// TODO Auto-generated method stub
		return false;
	}

	public int relationshipsLength() {
		// TODO Auto-generated method stub
		return 0;
	}


	
}