/*****************************************************************************
 * Source code information
 * -----------------------
 * Original author    Andrew Lum, University of Sydney
 * Author email       alum@it.usyd.edu.au
 * Package            SIV
 * Web                
 * Created            9 August 2003
 * Filename           $RCSfile: GraphSetup.java,v $
 * Revision           $Revision: 1.10 $
 * Release status     $State: Exp $
 *
 * Last modified on   $Date: 2003/09/22 07:24:16 $
 *               by   $Author: alum $
 *
 * ****************************************************************************/

package vlum.graph.model;

import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.rdf.model.*;

import java.util.*;

import vlum.graph.GraphSetup;

/**
 * 
 * @author $Author: alum $
 * @version CVS $Id: GraphSetup.java,v 1.10 2003/09/22 07:24:16 alum Exp $
 *
 * This class is a pre-configuration initialiser for the runtime graph model.
 * The relationship and display names are setup at this stage.
 */

public class OWLJenaSetup extends GraphSetup {

	//String[] displayIds;
	//String[] relationshipIds;

	final String MECUREO_NS =
		"http://www.it.usyd.edu.au/~alum/ontologies/mecureo#";

	public OWLJenaSetup(String uri) {
		try {
			// get ontology model
			OntModel model = ModelFactory.createOntologyModel();
			//FileInputStream fis = new FileInputStream("c:\\\\Programming\\chat.owl");
			//model.read(fis, null);
			model.read(uri);

			// get display
			int c = 1;
			displayIds = new String[c];
			for (int i = 0; i < c; i++) {
				String a = "Default View";
				if (a != null) {
					displayIds[i] = a;
				} else
					displayIds[i] = null;
			}
			/*
			int c = displaysLength();
			displayIds = new String[c];
			for (int i = 0; i < c; i++) {
			    String a = getAttrText(i, "ID");
			    if (a != null) {
			        displayIds[i] = a;
			    } else
			        displayIds[i] = null;
			}
			*/
			
			
			// get relationships
			getRelationships(model);

		} catch (Exception ex) {
			System.out.println(
				"Graph Setup Initialisation Problem: " + ex.toString());
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * Populate the relationship array with the names of the properties
	 * under the Mecureo namespace field.
	 * 
	 * @param model the ontology model that has the properties to extract
	 * 
	 */
	private void getRelationships(OntModel model) {
		Iterator it = model.listOntProperties();
		Vector buffer = new Vector();
		while (it.hasNext()) {
			OntProperty p = (OntProperty) it.next();
			if (p.getNameSpace().equals(MECUREO_NS)) {
				buffer.add(p.getLocalName());
			}
		}
		relationshipIds = new String[buffer.size()];
		for (int i = 0; i < buffer.size(); i++) {
			relationshipIds[i] = (String) (buffer.elementAt(i));
		}
	}

	/**
	 * 
	 * TODO: displayLength (current dummy set to return 1)
	 */
	public int displaysLength() {
		return displayIds.length;
	}
	
	
	public int getComparisonBase(int index) {
		if (!"comparison".equals(getDisplayType(index)))
			return -1;
		String d = getNodeText(index, "baseData");
		return getId(d);
	}
	
	
	public int getComparisonSubtractor(int index) {
		if (!"comparison".equals(getDisplayType(index)))
			return -1;
		String d = getNodeText(index, "compareWith");
		return getId(d);
	}
	
	
	public String getDisplayType(int index) {
		//return getAttrText(index, "type");
		return "plain";
	}
	
	
	public int getId(String s) {
		for (int i = 0; i < displayIds.length; i++) {
			if (s.equals(displayIds[i]))
				return i;
		}
		return -1;
	}
	
	
	public String getNodeName(int index) {
		return getNodeText(index, "name");
	}
	
	
	public String getNodeReliabilityTag(int index) {
		return getNodeText(index, "reliabilityTag");
	}
	
	
	public String getNodeTag(int index) {
		return getNodeText(index, "tag");
	}
	
	
	/*private*/
	public String getNodeText(int index, String s) {
		/*
		try {
			NodeList l =
				setupDocument.getDocumentElement().getElementsByTagName(
					"display");
			NodeList nl = l.item(index).getChildNodes();
			for (int j = 0; j < nl.getLength(); j++) {
				String nodeName = nl.item(j).getNodeName();
				if (s.equals(nodeName)) {
					String nval = nl.item(j).getFirstChild().getNodeValue();
					return nval;
				}
			}
		} catch (Exception ex) {
		}
		*/
		return "";
	}

	public String getNodeToolTip(int index) {
		return getNodeText(index, "tooltip");
	}
	public String getRelationshipType(int index) {
		try {
			return relationshipIds[index];
		} catch (Exception e) {
			return null;
		}
	}
	
	public int getViewNameIndex(String s) {
		/*
		try {
			NodeList l =
				setupDocument.getDocumentElement().getElementsByTagName(
					"display");
			for (int i = 0; i < l.getLength(); i++) {
				NodeList nl = l.item(i).getChildNodes();
				for (int j = 0; j < nl.getLength(); j++) {
					String nodeName = nl.item(j).getNodeName();
					if ("name".equals(nodeName)) {
						String nval = nl.item(j).getFirstChild().getNodeValue();
						if (s.equals(nval))
							return i;
					}
				}
			}
		} catch (Exception ex) {
		}
		return -1;
		*/
		return 0;
	}
	
	public int getViewTagIndex(String s) {
		/*
		try {
			NodeList l =
				setupDocument.getDocumentElement().getElementsByTagName(
					"display");
			for (int i = 0; i < l.getLength(); i++) {
				NodeList nl = l.item(i).getChildNodes();
				for (int j = 0; j < nl.getLength(); j++) {
					String nodeName = nl.item(j).getNodeName();
					if ("tag".equals(nodeName)) {
						String nval = nl.item(j).getFirstChild().getNodeValue();
						if (s.equals(nval))
							return i;
					}
				}
			}
		} catch (Exception ex) {
		}
		return -1;
		*/
		return 0;
	}
	
	public boolean isAdjustable(int index) {
		//String a = getAttrText(index, "adjustable"); 
		String a = null;
		return (a == null || a.length() == 0 || "true".equals(a));
	}
	
	public boolean isComparison(int index) {
		//String a = getAttrText(index, "type");
		String a = null;
		return (a != null && a.length() != 0 && "comparison".equals(a));
	}
	
	public boolean isViewable(int index) {
		//String a = getAttrText(index, "view");
		String a = null;
		return (a == null || a.length() == 0 || "true".equals(a));
	}
	public int relationshipsLength() {
		return relationshipIds.length;
	}
	
}