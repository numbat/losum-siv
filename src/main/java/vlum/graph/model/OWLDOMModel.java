package vlum.graph.model;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.ProgressMonitor;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import vlum.graph.GenericGraphNode;
import vlum.graph.GenericGraphPeer;
import vlum.graph.GraphModel;
import vlum.graph.GraphSetup;

/**
 * Object based implementation of the GraphModel class using basic XML parser.
 * 
 * @author: Andrew Lum
 */
public class OWLDOMModel extends GraphModel {

	private static final String DC_TITLE = "http://purl.org/dc/elements/1.1/Title";
	private static final int maxNodeCount = 2048;
	
	public OWLDOMModel(GraphSetup s, String uri) throws Exception {
		this(s, uri, null);
	}
	public OWLDOMModel(GraphSetup s, String uri, Component parentFrame)
		throws Exception {

		int doclen;

		// get ontology model
		try {
			timestamp("start");
			memorystamp("start");
			
			timestamp("start dom parsing");
			DOMParser parser = new DOMParser();
			parser.parse(uri);

			Document myDocument = parser.getDocument();
			NodeList resultList = myDocument.getElementsByTagName("Concept");

			markCount = s.displaysLength(); // number of views for this graph

			doclen = resultList.getLength();

			// set up storage of graph data
			nameMap = new Hashtable();
			aboutMap = new Hashtable();
			nodes = new GenericGraphNode[doclen]; // initialise node list

			modelSize = 0;
			
			ProgressMonitor progressMonitor = new ProgressMonitor(parentFrame, "Loading Ontology", "", 0, doclen);
			progressMonitor.setProgress(0);
			progressMonitor.setMillisToDecideToPopup(20);
			progressMonitor.setNote("generating graph");

			timestamp("start building nodes");
			for (int i = 0; i < doclen; i++) {
				progressMonitor.setProgress(i);
				GenericGraphNode myNode = new GenericGraphNode(markCount);
				nodes[i] = myNode;
				modelSize++;
				
				// temporary storage for peers
				ArrayList myPeers = new ArrayList();
				
				// get name and description
				Element thisElement = (Element) resultList.item(i);
				String desc = "#" + thisElement.getAttribute("rdf:ID");
				myNode.setDescription(desc);
				//System.out.println("Description: " + desc);
				
				NodeList childList = thisElement.getChildNodes();
				for (int ii = 0; ii < childList.getLength(); ii++) {
					Node myChild = (Node) childList.item(ii);
					String nodeType = myChild.getNodeName();
					boolean nodeValue = myChild.hasAttributes();
					if (nodeType.equals("dc:Title")) {
						// set the title
						myNode.setName(myChild.getFirstChild().getNodeValue());
						//System.out.println("\tDisplay Title: " + myChild.getFirstChild().getNodeValue());
					} else if (childList.item(ii).hasAttributes()) {
						String resource = myChild.getAttributes().getNamedItem("rdf:resource").getNodeValue();
						GenericGraphPeer myPeer = new GenericGraphPeer(myNode.getDescription(), resource, nodeType);
						//System.out.println("\tPeer: " + myNode.getDescription() + "-----" + resource + "-----" + nodeType);
						myPeers.add(myPeer);
					}
				}
				// store the peers as an array
				Object[] myArray = myPeers.toArray();
				myNode.setPeers(myArray);
				
				// get the node type
				myNode.setType("No Type");

				// map the name and descriptions in the hash tables
				nameMap.put(myNode.getName(), new Integer(i));
				aboutMap.put(myNode.getDescription(), new Integer(i));

				/*
				 * Artifacts from the original VlUM to set the score and certaintly.
				 * It is redundant now in this version because GenericGraphNode sets
				 * the default score and certainties automatically.
				 * 
				
				// set the default scores and certainties
				// NOTE: maybe move this to the Node class.
				Vector marksToDo = new Vector();
				for (int j = 0; j < markCount; j++) {
					myNode.setScore(j, Float.MIN_VALUE);
					myNode.setNormalisedScore(j, Float.MIN_VALUE);
					myNode.setCertainty(j, 0f);
					myNode.setNormalisedCertainty(j, 0f);
					marksToDo.addElement(new Integer(j));
				}

				// set the scores and certainties
				for (int m = 0; m < 1; m++) {
					String mk = "0.000";
					String rl = "0.000";
					int mindex = 0;
					marksToDo.removeElement(new Integer(mindex));
					// set the score
					myNode.setScore(mindex, (new Float(mk)).floatValue());
				}
				*/
				
			}
			
			//System.out.println("Weaving Peers");

			timestamp("weave peers");
			progressMonitor.setNote("weaving peers");
			progressMonitor.setProgress(0);

			// now weave the peers
			peers = new int[doclen][];
			for (int i = 0; i < doclen; i++) {
				progressMonitor.setProgress(doclen);
				Vector p = nodes[i].getPeers();
				peers[i] = new int[p.size()];
				for (int j = 0; j < p.size(); j++) {
					GenericGraphPeer myPeer = (GenericGraphPeer) nodes[i].getPeers().elementAt(j);
					peers[i][j] = ((Integer) aboutMap.get(myPeer.getEndNode())).intValue();
				}
			}
			progressMonitor.close();
			
			timestamp("end");
			memorystamp("end");
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
}
