package vlum.graph.model;

import java.awt.Component;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;
import java.lang.reflect.*;

import qdxml.DocHandler;
import qdxml.QDParser;

import vlum.graph.GenericGraphNode;
import vlum.graph.GenericGraphPeer;
import vlum.graph.GraphModel;
import vlum.graph.GraphSetup;

public class SKOSQDXMLModel extends GraphModel implements DocHandler  {

    private GenericGraphNode currentNode;
    private GenericGraphPeer currentPeer;
    private ArrayList tempNodes;
    private ArrayList currentPeers;
    private Object currentObject;
	private Method currentMethod;
	
	public SKOSQDXMLModel(GraphSetup s, String uri) throws Exception {
		this(s, uri, null);
	}
	
	public SKOSQDXMLModel(GraphSetup s, String uri, Component parentFrame) throws Exception {
        timestamp("start");
        memorystamp("start");
        
		// initialise object graph attributes
        nameMap = new Hashtable();
        aboutMap = new Hashtable();
        modelSize = 0;
        markCount = 1;
		
        // initialise parsing attributes
        tempNodes = new ArrayList();
        currentPeers = new ArrayList();
        currentNode = new GenericGraphNode();
        
        // run the QDXML Parser to build nameMap, aboutMap, and tempNodes
        timestamp("start fetch file");
        String data = fetch(uri);
        StringReader reader = new StringReader(data);
        timestamp("start parsing");
        QDParser.parse(this, reader);
        
        // transfer nodes from ArrayList tempNodes[] to Array nodes[]
        timestamp("start transfer nodes");
        Object[] myNodes = tempNodes.toArray();
        nodes = new GenericGraphNode[myNodes.length];        
        for (int i=0; i< myNodes.length; i++) {
            currentNode = (GenericGraphNode) myNodes[i];
            nodes[i] = currentNode;
        }
		
        // now weave the peers
        timestamp("weave peers");
		peers = new int[nodes.length][];
		for (int i = 0; i < nodes.length; i++) {
			Vector p = nodes[i].getPeers();
			peers[i] = new int[p.size()];
			for (int j = 0; j < p.size(); j++) {
				GenericGraphPeer myPeer = (GenericGraphPeer) nodes[i].getPeers().elementAt(j);
				peers[i][j] = ((Integer) aboutMap.get(myPeer.getEndNode())).intValue();
			}
		}
        timestamp("end");
        memorystamp("end");
        
	}

	private void setCurrentNodeMethod(String name) {
		Class c = GenericGraphNode.class;
    	Method ms[] = c.getMethods();
    	for (int i = 0; i < ms.length; i++) {
			if (ms[i].getName().compareTo(name) == 0)
				currentMethod = ms[i];
		}
    	currentObject = currentNode;
	}

	private void setCurrentPeerMethod(String name) {
		Class c = GenericGraphPeer.class;
    	Method ms[] = c.getMethods();
    	for (int i = 0; i < ms.length; i++) {
			if (ms[i].getName().compareTo(name) == 0)
				currentMethod = ms[i];
		}
    	currentObject = currentPeer;
	}
	
    /***************************************************************************
     * Implementation of DocHandler is below this line
     *  
     **************************************************************************/
	
	public void startElement(String tag, Hashtable h) throws Exception {
		//System.out.println(":::" + tag);
		// we only consider the elements here, omitting everthing else
		if (tag.equals("skos:Concept")) {
            String desc = "#" + h.get("rdf:about");
            currentNode.setDescription(desc);
        }
		if (tag.equals("skos:prefLabel")) {
        	setCurrentNodeMethod("setName");
        }
        if (tag.equals("skos:definition")) {
        	setCurrentNodeMethod("setDefinition");
        }
        if (tag.equals("sago:cweight")) {
        	setCurrentNodeMethod("setWeight");
        }
        // peer related tags
        if (tag.equals("skos:broader")) {
            String resource = (String) h.get("rdf:resource");
            currentPeer = new GenericGraphPeer(currentNode.getDescription(), resource, tag);
            currentPeers.add(currentPeer);
        }
        if (tag.equals("skos:narrower")) {
            String resource = (String) h.get("rdf:resource");
            currentPeer = new GenericGraphPeer(currentNode.getDescription(), resource, tag);
            currentPeers.add(currentPeer);
        }
        if (tag.equals("skos:related")) {
            String resource = (String) h.get("rdf:resource");
            currentPeer = new GenericGraphPeer(currentNode.getDescription(), resource, tag);
            currentPeers.add(currentPeer);
        }
        if (tag.equals("sago:strength")) {
        	setCurrentPeerMethod("setStrength");
        }
        if (tag.equals("sago:weight")) {
        	setCurrentPeerMethod("setWeight");
        }
        if (tag.equals("sago:type")) {
        	setCurrentPeerMethod("setType");
        }
        if (tag.equals("sago:keyword")) {
        	String source = "#" + (String) h.get("sago:source");
        	currentPeer.setSource(source);
        	setCurrentPeerMethod("setKeyword");
        }
	}

	public void endElement(String tag) throws Exception {
        if (tag.equals("skos:Concept")) {
            // first save the current object to tempNodes
            currentNode.setType("No Type");
            Object[] myArray = currentPeers.toArray();
        	currentNode.setPeers(myArray);
        	tempNodes.add(currentNode);
        	nameMap.put(currentNode.getName(), new Integer(modelSize));
        	aboutMap.put(currentNode.getDescription(), new Integer(modelSize));
        	// now reset the current objects
        	currentNode = new GenericGraphNode();
        	currentPeers = new ArrayList();
        	modelSize++;
        }
	}

	public void startDocument() throws Exception {
        //System.out.println(" start document");
		
	}

	public void endDocument() throws Exception {
        //System.out.println(" end document");
		
	}

	public void text(String str) throws Exception {
		try {
			if (currentMethod != null) {
				Object a[] = new Object[1];
			  	a[0] = str;
				currentMethod.invoke(currentObject, a);
				currentMethod = null;	// reset current and class
			} else {
				currentMethod = null;	// reset current and class
				//System.out.println("Element not saved: " + str);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
    public static String fetch(String myUrl) throws Exception {
        URL url = new URL(myUrl);
        // try opening the URL
        URLConnection urlConnection = url.openConnection();
        urlConnection.setAllowUserInteraction(false);
        InputStream urlStream = url.openStream();
        //String type = URLConnection.guessContentTypeFromStream(urlStream);

        // search the input stream for links
        // first, read in the entire URL
        byte b[] = new byte[100000];
        int numRead = urlStream.read(b);
        String content = new String(b, 0, numRead);
        while (numRead != -1) {
            numRead = urlStream.read(b);
            if (numRead != -1) {
                String newContent = new String(b, 0, numRead);
                content += newContent;
            }
        }
        urlStream.close();
        return (content);
    }
    
	public static void main(String[] args) throws Exception {
		try {
			String ontologyUrl = "http://aefallen/ontologies/soft2130.skos";
			SKOSQDXMLSetup myGraphSetup = new SKOSQDXMLSetup(ontologyUrl);
			SKOSQDXMLModel myGraphModel = new SKOSQDXMLModel(myGraphSetup, ontologyUrl);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
}
