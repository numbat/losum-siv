/*****************************************************************************
 * Source code information
 * -----------------------
 * Original author    Andrew Lum, University of Sydney
 * Author email       alum@it.usyd.edu.au
 * Package            SIV
 * Web                
 * Created            9 August 2003
 * Filename           $RCSfile: OWLGraphModel.java,v $
 * Revision           $Revision: 1.14 $
 * Release status     $State: Exp $
 *
 * Last modified on   $Date: 2003/10/03 05:13:31 $
 *               by   $Author: alum $
 *
 * ****************************************************************************/

package vlum.graph.model;

import java.util.*;
import java.awt.*;

import javax.swing.ProgressMonitor;

import vlum.graph.GenericGraphNode;
import vlum.graph.GenericGraphPeer;
import vlum.graph.GraphModel;
import vlum.graph.GraphSetup;

import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.graph.*;

import mecureo.output.parser.*;

/**
 * Object based implementation of the GraphModel class.
 * Creation date: (18/06/2002 9:53:04 AM)
 *
 * @author: Andrew Lum
 */
public class OWLJenaModel extends GraphModel {

	private static final String DC_TITLE = "http://purl.org/dc/elements/1.1/Title";
	//private static final int maxNodeCount = 2048;


	public OWLJenaModel(GraphSetup s, String uri) throws Exception {
		this(s, uri, null);
	}
	public OWLJenaModel(GraphSetup s, String uri, Component parentFrame)
		throws Exception {

		int doclen;

		// get ontology model
		try {

			timestamp("start");
			memorystamp("start");
			
			timestamp("start jena fetch and parse");			
			OntModel model = ModelFactory.createOntologyModel();
			model.read(uri);
			
			markCount = s.displaysLength(); // number of views for this graph

			Iterator it = model.listIndividuals();
			Vector buffer = new Vector();
			
			timestamp("start building nodes");
			while (it.hasNext()) {
				Individual i = (Individual) it.next();
				buffer.add(i);
			}

			doclen = buffer.size();

			nameMap = new Hashtable();
			aboutMap = new Hashtable();
			nodes = new GenericGraphNode[doclen]; // initialise node list

			modelSize = 0;
			
			ProgressMonitor progressMonitor = new ProgressMonitor(parentFrame, "Loading Ontology", "", 0, doclen);
			progressMonitor.setProgress(0);
			progressMonitor.setMillisToDecideToPopup(20);
			progressMonitor.setNote("generating graph");

			for (int i = 0; i < doclen; i++) {
				progressMonitor.setProgress(i);
				GenericGraphNode myNode = new GenericGraphNode(markCount);
				nodes[i] = myNode;
				modelSize++;
				
				// temporary storage for peers
				ArrayList myPeers = new ArrayList();
				
				// get name and description
				Individual ind = (Individual) buffer.elementAt(i);
				myNode.setDescription(ind.toString());
				//System.out.println("Description: " + ind.toString());
				it = ind.listProperties();
				while (it.hasNext()) {
					Statement st = (Statement) it.next();
					Triple t = st.asTriple();
					//Node sub = t.getSubject();
					Node pre = t.getPredicate();
					Node obj = t.getObject();
					// if title...
					if (pre.getURI().equals(DC_TITLE)) {
						myNode.setName(obj.toString());
						//System.out.println("\tDisplay Title: " + obj.toString());
					}
					// if peer...
					if (InputOWL.getMecureoProperty(pre.getURI()) != null) {
						String peerType = InputOWL.getMecureoProperty(pre.getURI());
						String resource = obj.toString();
						//myNode.addPeer(resource, peerType);
						GenericGraphPeer myPeer = new GenericGraphPeer(myNode.getDescription(), resource, peerType);
						//System.out.println("\tPeer: " + myNode.getDescription() + "-----" + resource + "-----" + peerType);
						myPeers.add(myPeer);
					}
				}
				// store the peers as an array
				Object[] myArray = myPeers.toArray();
				myNode.setPeers(myArray);
				
				// get the node type
				myNode.setType("No Type");

				// map the name and descriptions in the hash tables
				nameMap.put(myNode.getName(), new Integer(i));
				aboutMap.put(myNode.getDescription(), new Integer(i));

			}
			
			timestamp("weave peers");
			// now weave the peers
			progressMonitor.setNote("weaving peers");
			progressMonitor.setProgress(0);
			peers = new int[doclen][];
			for (int i = 0; i < doclen; i++) {
				progressMonitor.setProgress(doclen);
				Vector p = nodes[i].getPeers();
				peers[i] = new int[p.size()];
				for (int j = 0; j < p.size(); j++) {
					GenericGraphPeer myPeer = (GenericGraphPeer) nodes[i].getPeers().elementAt(j);
					peers[i][j] = ((Integer) aboutMap.get(myPeer.getEndNode())).intValue();
				}
			}
			
			progressMonitor.close();
			
			timestamp("end");
			memorystamp("end");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
