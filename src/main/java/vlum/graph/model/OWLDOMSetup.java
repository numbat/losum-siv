
package vlum.graph.model;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import vlum.graph.GraphSetup;

public class OWLDOMSetup extends GraphSetup {

	//String[] displayIds;
	//String[] relationshipIds;

	final String MECUREO_NS = "http://www.it.usyd.edu.au/~alum/ontologies/mecureo#";

	public OWLDOMSetup(String uri) {
		try {

			DOMParser parser = new DOMParser();
			//parser.parse(argv[0]);
			parser.parse("http://pc0-g61-2.it.usyd.edu.au/ontologies/uidp.owl");
			Document myDocument = parser.getDocument();
			// get display
			int c = 1;
			displayIds = new String[c];
			for (int i = 0; i < c; i++) {
				String a = "Default View";
				if (a != null) {
					displayIds[i] = a;
				} else
					displayIds[i] = null;
			}

			// get relationships
			getRelationships(myDocument);

		} catch (Exception ex) {
			System.out.println("Graph Setup Initialisation Problem: "
					+ ex.toString());
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * Populate the relationship array with the names of the properties under
	 * the Mecureo namespace field.
	 * 
	 * @param model
	 *            the ontology model that has the properties to extract
	 *  
	 */
	private void getRelationships(Document myDocument) {
		NodeList resultList = myDocument
				.getElementsByTagName("owl:ObjectProperty");
		relationshipIds = new String[resultList.getLength()];
		for (int i = 0; i < resultList.getLength(); i++) {
			Element thisElement = (Element) resultList.item(i);
			relationshipIds[i] = thisElement.getAttribute("rdf:ID");
		}
	}

	/**
	 * 
	 * displayLength (current dummy set to return 1)
	 */
	public int displaysLength() {
		return displayIds.length;
	}

	public int getComparisonBase(int index) {
		if (!"comparison".equals(getDisplayType(index)))
			return -1;
		String d = getNodeText(index, "baseData");
		return getId(d);
	}

	public int getComparisonSubtractor(int index) {
		if (!"comparison".equals(getDisplayType(index)))
			return -1;
		String d = getNodeText(index, "compareWith");
		return getId(d);
	}

	public String getDisplayType(int index) {
		//return getAttrText(index, "type");
		return "plain";
	}

	public int getId(String s) {
		for (int i = 0; i < displayIds.length; i++) {
			if (s.equals(displayIds[i]))
				return i;
		}
		return -1;
	}

	public String getNodeName(int index) {
		return getNodeText(index, "name");
	}

	public String getNodeReliabilityTag(int index) {
		return getNodeText(index, "reliabilityTag");
	}

	public String getNodeTag(int index) {
		return getNodeText(index, "tag");
	}

	/* private */
	public String getNodeText(int index, String s) {
		/*
		 * try { NodeList l =
		 * setupDocument.getDocumentElement().getElementsByTagName( "display");
		 * NodeList nl = l.item(index).getChildNodes(); for (int j = 0; j <
		 * nl.getLength(); j++) { String nodeName = nl.item(j).getNodeName(); if
		 * (s.equals(nodeName)) { String nval =
		 * nl.item(j).getFirstChild().getNodeValue(); return nval; } } } catch
		 * (Exception ex) { }
		 */
		return "";
	}

	public String getNodeToolTip(int index) {
		return getNodeText(index, "tooltip");
	}

	public String getRelationshipType(int index) {
		try {
			return relationshipIds[index];
		} catch (Exception e) {
			return null;
		}
	}

	public int getViewNameIndex(String s) {
		/*
		 * try { NodeList l =
		 * setupDocument.getDocumentElement().getElementsByTagName( "display");
		 * for (int i = 0; i < l.getLength(); i++) { NodeList nl =
		 * l.item(i).getChildNodes(); for (int j = 0; j < nl.getLength(); j++) {
		 * String nodeName = nl.item(j).getNodeName(); if
		 * ("name".equals(nodeName)) { String nval =
		 * nl.item(j).getFirstChild().getNodeValue(); if (s.equals(nval)) return
		 * i; } } } } catch (Exception ex) { } return -1;
		 */
		return 0;
	}

	public int getViewTagIndex(String s) {
		/*
		 * try { NodeList l =
		 * setupDocument.getDocumentElement().getElementsByTagName( "display");
		 * for (int i = 0; i < l.getLength(); i++) { NodeList nl =
		 * l.item(i).getChildNodes(); for (int j = 0; j < nl.getLength(); j++) {
		 * String nodeName = nl.item(j).getNodeName(); if
		 * ("tag".equals(nodeName)) { String nval =
		 * nl.item(j).getFirstChild().getNodeValue(); if (s.equals(nval)) return
		 * i; } } } } catch (Exception ex) { } return -1;
		 */
		return 0;
	}

	public boolean isAdjustable(int index) {
		//String a = getAttrText(index, "adjustable");
		String a = null;
		return (a == null || a.length() == 0 || "true".equals(a));
	}

	public boolean isComparison(int index) {
		//String a = getAttrText(index, "type");
		String a = null;
		return (a != null && a.length() != 0 && "comparison".equals(a));
	}

	public boolean isViewable(int index) {
		//String a = getAttrText(index, "view");
		String a = null;
		return (a == null || a.length() == 0 || "true".equals(a));
	}

	public int relationshipsLength() {
		return relationshipIds.length;
	}

}