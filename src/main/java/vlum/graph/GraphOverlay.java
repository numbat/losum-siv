/*
 * Created on 12/12/2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package vlum.graph;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Vector;

/**
 * @author alum
 *         <p/>
 *         To change the template for this generated type comment go to
 *         Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GraphOverlay {
    private GraphModel myGraphModel;
    private Vector changedTitles;

    public GraphOverlay(GraphModel myGraph) {
        myGraphModel = myGraph;
        changedTitles = new Vector();
        return;
    }

    /**
     * Parse the overlay XML file and overlay the ontology.
     *
     * @param uri the location of the overlay
     */
    public void parse(String uri) {
        DOMParser parser = new DOMParser();
        try {
            parser.parse(uri);
            Document myDocument = parser.getDocument();
            NodeList resultList = myDocument.getElementsByTagName("result");
            for (int i = 0; i < resultList.getLength(); i++) {
                String title = GraphOverlay.toRDFTitle(((Element) resultList.item(i)).getAttribute("concept"));
                String score = ((Element) resultList.item(i)).getAttribute("score");
                String certainty = ((Element) resultList.item(i)).getAttribute("confidence");
                if (myGraphModel.getTitleIndex(title) != -1) {
                    myGraphModel.setMark(title, 0, new Float(score).floatValue());
                    myGraphModel.setNormalisedMark(title, 0, new Float(score).floatValue());
                    myGraphModel.setReliability(title, 0, new Float(certainty).floatValue());
                    myGraphModel.setNormalisedReliability(title, 0, new Float(certainty).floatValue());
                    changedTitles.addElement(title);
                }
            }
        } catch (Exception e) {
            System.out.println("Error parsing overlay");
        }
    }

    /**
     * Reset the score and confidence data to zero.
     */
    public void clear() {
        for (int i = 0; i < changedTitles.size(); i++) {
            myGraphModel.setMark((String) changedTitles.elementAt(i), 0, 0f);
            myGraphModel.setNormalisedMark((String) changedTitles.elementAt(i), 0, 0f);
            myGraphModel.setReliability((String) changedTitles.elementAt(i), 0, 0f);
            myGraphModel.setNormalisedReliability((String) changedTitles.elementAt(i), 0, 0f);
        }
        changedTitles = new Vector();
    }

    private static String toRDFTitle(String key) {
        StringBuffer cur = new StringBuffer();
        //replace ampersands with &amp; and remove quotation marks
        for (int i = 0; i < key.length(); ++i) {
            char c = key.charAt(i);
            switch (c) {
                case '&':
                    cur.append("&amp;");
                    break;
                case '"':
                    break;
                default:
                    if (c > 127) {
                        cur.append("%" + Integer.toHexString((int) c));
                    } else {
                        cur.append(c);
                    }
            }
        }
        return cur.toString();
    }

    public static void main(String argv[]) {
        System.out.println("Hello!");
    }

}
