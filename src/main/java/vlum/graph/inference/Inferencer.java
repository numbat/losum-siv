/*
 * Created on 15/10/2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package vlum.graph.inference;

import vlum.graph.*;

/**
 * @author alum
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public abstract class Inferencer {

	protected long[] depthGenerationMap;
	protected int maxDepth;
	protected GraphModel myGraphModel;
	protected String relationshipFilter;

	/**
	 * 
	 */
	public Inferencer(GraphModel myGraph) {
		myGraphModel = myGraph;
		depthGenerationMap = new long[myGraph.size()];
	}

	public float doInference(String title, int depthLimit, String filter) {
		relationshipFilter = filter;
		float val = doInference(title, depthLimit);
		relationshipFilter = null;
		return val;
	}

	public abstract float doInference(String title, int depthLimit);
}
