/*
 * Created on 15/10/2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package vlum.graph.inference;

import vlum.graph.*;

/**
 * @author alum
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class Averager extends Inferencer {

	/**
	 * 
	 */
	public Averager(GraphModel myGraph) {
		super(myGraph);
	}

	public float doInference(String title, int depthLimit) {
		for (int i = 0; i < depthGenerationMap.length; i++) {
			depthGenerationMap[i] = -1;
		}
		maxDepth = depthLimit;
		float result = recurse(myGraphModel.getTitleIndex(title), 0);
		return result;
	}
	
	private float recurse(int root, int curDepth)  {
		int peers[] = myGraphModel.getPeers(root, relationshipFilter);
		float result_node = myGraphModel.getNormalisedMark(root, 0);
		float result_peers = 0f;		
			
		depthGenerationMap[root] = 1;
		
		if (peers.length != 0) {
			for (int i = 0; i < peers.length; i++) {
				if ((depthGenerationMap[peers[i]] != 1) && (curDepth < maxDepth)) {
					result_peers = result_peers + recurse(peers[i], curDepth + 1);
				}
			}
			result_peers = result_peers/peers.length;
		}
		return result_node + (1f - result_node)*result_peers;
	}
	

}
