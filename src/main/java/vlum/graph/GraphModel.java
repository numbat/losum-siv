/*****************************************************************************
 * Source code information
 * -----------------------
 * Original author    Andrew Lum, University of Sydney
 * Author email       alum@it.usyd.edu.au
 * Package            Andrew
 * Web                
 * Created            9 August 2003
 * Filename           $RCSfile: GraphModel.java,v $
 * Revision           $Revision: 1.6 $
 * Release status     $State: Exp $
 *
 * Last modified on   $Date: 2003/10/03 05:13:32 $
 *               by   $Author: alum $
 *
 * ****************************************************************************/

package vlum.graph;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Vector;

import vlum.graph.overlay.GraphOverlayColour;
import vlum.graph.overlay.GraphOverlayHorizontal;
import vlum.graph.overlay.GraphOverlayInference;
import vlum.graph.overlay.QDXMLGraphOverlayColour;
import vlum.graph.overlay.QDXMLGraphOverlayHorizontal;

/**
 * GraphModel abstract class with method definitions.
 * Creation date: (4/07/2002 11:59:48 AM)
 * @author: Andrew Lum
 */
public abstract class GraphModel {
	
	protected int markCount;
	protected int modelSize;
	
	protected Hashtable nameMap;
	protected Hashtable aboutMap; // preserve order
	
	protected GenericGraphNode[] nodes;
	
	// for quick access
	protected int[][] peers;
	
	public GraphOverlayHorizontal myOverlayHorizontal;
	public GraphOverlayColour myOverlayColour;
	public GraphOverlayInference myOverlayInference;
	
	protected Component myParentFrame;
	
	/**
	 * Get the mark count.
	 * Creation date: (28/06/2002 12:56:24 PM)
	 * @return int
	 */
	public int getMarkCount() {
		return markCount;
	}
	
	
	public int getAboutIndex(String title) {
		try {
			return ((Integer) aboutMap.get(title)).intValue();
		} catch (Exception e) {
			return -1;
		}
	}
	public float getMark(int index, int dataset) {
		try {
			return nodes[index].getScore(dataset);
		} catch (Exception e) {
			return -1f;
		}
	}
	public float getNormalisedMark(int index, int dataset) {
		try {
			return nodes[index].getNormalisedScore(dataset);
		} catch (Exception e) {
			return -1f;
		}
	}
	public float getNormalisedReliability(int index, int dataset) {
		try {
			return nodes[index].getNormalisedCertainty(dataset);
		} catch (Exception e) {
			return -1f;
		}
	}
	public int[] getPeers(int index) {
		return peers[index];
	}

	/**
	 * Get peers of a certain type.
	 * Creation date: (08/07/2002 15:07:20 AM)
	 */
	public int[] getPeers(int index, String type) {
		Vector peerVector = nodes[index].getPeers(type);
		int[] peerArray = new int[peerVector.size()];
		// convert GenericGraphPeer objects to int values
		for (int i = 0; i < peerVector.size(); i++) {
			String key =
				((GenericGraphPeer) peerVector.elementAt(i)).getEndNode();
			peerArray[i] = ((Integer) aboutMap.get(key)).intValue();
		}
		return peerArray;
	}
	
	public float getReliability(int index, int dataset) {
		try {
			return nodes[index].getCertainty(dataset);
		} catch (Exception e) {
			return -1f;
		}
	}
	public String getResource(int index) {
		try {
			return nodes[index].getDescription();
		} catch (Exception e) {
			return null;
		}
	}
	public String getTitle(int index) {
		try {
			return nodes[index].getName();
		} catch (Exception e) {
			return null;
		}
	}

	public String getTitle(String str) {
		try {
			int index = new Integer(str).intValue();			
			return nodes[index].getName();
		} catch (Exception e) {
			return null;
		}
	}

	public int getTitleIndex(String title) {
		try {
			return ((Integer) nameMap.get(title)).intValue();
		} catch (Exception e) {
			return -1;
		}
	}
	
	/**
	 * getType method comment.
	 */
	public java.lang.String getType(int index) {
		try {
			return nodes[index].getType();
		} catch (Exception e) {
			return null;
		}
	}
	
	
	public int size() {
		return modelSize;
	}

	/**
	 * @author alum
	 * set the score for a node
	 */
	public void setMark(int index, int dataset, float score) {
		nodes[index].setScore(dataset, score);
	}
	
	/**
	 * @author alum
	 * set the score for a node
	 */
	public void setMark(String title, int dataset, float score) {
		nodes[getTitleIndex(title)].setScore(dataset, score);
	}

	/**
	 * @author alum
	 * set the score for a node
	 */
	public void setNormalisedMark(int index, int dataset, float nscore) {
		nodes[index].setNormalisedScore(dataset, nscore);
	}
	
	/**
	 * @author alum
	 * set the score for a node
	 */
	public void setNormalisedMark(String title, int dataset, float nscore) {
		nodes[getTitleIndex(title)].setNormalisedScore(dataset, nscore);
	}

	/**
	 * @author alum
	 * set the certainty for a node
	 */
	public void setReliability(int index, int dataset, float reliability) {
		nodes[index].setCertainty(dataset, reliability);
	}
	
	/**
	 * @author alum
	 * set the certainty for a node
	 */
	public void setReliability(String title, int dataset, float reliability) {
		nodes[getTitleIndex(title)].setCertainty(dataset, reliability);
	}

	/**
	 * @author alum
	 * set the certainty for a node
	 */
	public void setNormalisedReliability(int index, int dataset, float nreliability) {
		nodes[index].setNormalisedCertainty(dataset, nreliability);
	}
	
	/**
	 * @author alum
	 * set the certainty for a node
	 */
	public void setNormalisedReliability(String title, int dataset, float nreliability) {
		nodes[getTitleIndex(title)].setNormalisedCertainty(dataset, nreliability);
	}
	
	
	/**
	 * @return
	 */
	public Component getMyParentFrame() {
		return myParentFrame;
	}

	/**
	 * @param component
	 */
	public void setMyParentFrame(Component component) {
		myParentFrame = component;
	}

	/**
	 * @return
	 */
	public GraphOverlayColour getOverlayColour() {
		return myOverlayColour;
	}

	/**
	 * @return
	 */
	public GraphOverlayHorizontal getOverlayHorizontal() {
		return myOverlayHorizontal;
	}

	public GraphOverlayInference getOverlayInference() {
		return myOverlayInference;
	}

	/**
	 * @param colour
	 */
	public void setOverlayColour(String uri) {
		myOverlayColour = new QDXMLGraphOverlayColour(this);
		myOverlayColour.parse(uri);
	}

	/**
	 * @param horizontal
	 */
	public void setOverlayHorizontal(String uri) {
		myOverlayHorizontal = new QDXMLGraphOverlayHorizontal(this);
		myOverlayHorizontal.parse(uri);
	}
	
	/**
	 *  @param inference
	 */
	public void setOverlayInference(String uri) {
		myOverlayInference = new GraphOverlayInference(this);
	}
	
	protected void timestamp(String msg) {
	     Calendar now = Calendar.getInstance();
	     SimpleDateFormat formatter = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
	     System.out.println("Current time = " + formatter.format(now.getTime()) + " (" + msg + ")");
	}
	
	protected void memorystamp(String msg) {
		System.out.println("Java memory in use = " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) + " (" + msg + ")");
	}

	public GenericGraphNode getNodeObject(String title) {
		return (nodes[getTitleIndex(title)]);
	}
	
}
