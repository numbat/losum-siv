/*****************************************************************************
 * Source code information
 * -----------------------
 * Original author    Andrew Lum, University of Sydney
 * Author email       alum@it.usyd.edu.au
 * Package            Andrew
 * Web                
 * Created            9 August 2003
 * Filename           $RCSfile: GenericGraphPeer.java,v $
 * Revision           $Revision: 1.3 $
 * Release status     $State: Exp $
 *
 * Last modified on   $Date: 2003/09/08 23:52:00 $
 *               by   $Author: alum $
 *
 * ****************************************************************************/

package vlum.graph;

/**
 * This class defines a peer in the directed graph. It holds
 * a description of the peer relationship as well.
 *
 * Creation date: (18/06/2002 10:38:34 AM)
 * @author: Andrew Lum
 */

public class GenericGraphPeer {
	private String startNode;
	private String endNode;
	private String type = "";				// e.g. the Mecureo descriptor such as "parent"
	private String description = "";		// e.g. the SKOS type such as "broader"
	private String strength = "";			// e.g. the Mecureo strength such as "strong"
	private String weight = "";				// e.g. the Mecureo link weighting such as 0.566
    private String keyword = "";			// e.g. the Mecureo discovered keyword from definition such as "system"
    private String source = "";				// e.g. the Mecureo source of the discoved keyword
    


	/**
	 * GenericGraphPeer constructor.
	 */
	public GenericGraphPeer(String start, String end, String des) {
		startNode = start;
		endNode = end;
		description = des;
	}
	
	/**
	 * Get description.
	 * Creation date: (18/06/2002 11:43:41 AM)
	 * @return java.lang.String
	 */
	public java.lang.String getDescription() {
		return description;
	}
	/**
	 * getEndNode.
	 * Creation date: (18/06/2002 11:43:41 AM)
	 * @return java.lang.String
	 */
	public java.lang.String getEndNode() {
		return endNode;
	}
	/**
	 * Get startNode.
	 * Creation date: (18/06/2002 11:43:41 AM)
	 * @return java.lang.String
	 */
	public java.lang.String getStartNode() {
		return startNode;
	}
	/**
	 * Assign node description.
	 * Creation date: (18/06/2002 11:43:41 AM)
	 * @param newDescription java.lang.String
	 */
	public void setDescription(java.lang.String newDescription) {
		description = newDescription;
	}
	/**
	 * Assign endNode.
	 * Creation date: (18/06/2002 11:43:41 AM)
	 * @param newEndNode String
	 */
	public void setEndNode(java.lang.String newEndNode) {
		endNode = newEndNode;
	}
	/**
	 * Assign startNode.
	 * Creation date: (18/06/2002 11:43:41 AM)
	 * @param newStartNode String
	 */
	public void setStartNode(java.lang.String newStartNode) {
		startNode = newStartNode;
	}
	public String getStrength() {
		return strength;
	}
	public void setStrength(String strength) {
		this.strength = strength;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
}
