package vlum;

import java.applet.AppletContext;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;

import vlum.graph.GenericGraphNode;
import vlum.graph.GenericGraphPeer;
import vlum.graph.GraphModel;
import vlum.graph.GraphSetup;
import vlum.graph.overlay.GraphOverlayColour;

/**
 * Insert the type's description here.
 * Creation date: (4/04/2003 11:23:49 AM)
 *
 * @author:
 */
public class ContextData {
    // initial parameters

    private String contentBaseUrl = "";
    private String ontologyUrl = "";
    private String helpUrl = "";
    private String baseUrl = "";
    private String initialTitle = "";
    private String userUrl = "";
    private String setupUrl = "";

    private String colourUrl = "";
    private String horizontalUrl = "";

    private String user = "";

    private int depthLimit = -1;

    // runtime data
    private Vector history;

    // click stream
    private String clickstream = "";

    // applet info
    private AppletContext cntx;

    // graph info
    private GraphSetup setup;
    private GraphModel graphModel;

    /**
     * ContextData constructor comment.
     */
    public ContextData() {
        history = new Vector();
    }

    /**
     * Insert the method's description here.
     * Creation date: (15/04/2003 11:52:33 AM)
     *
     * @return java.applet.AppletContext
     */
    public java.applet.AppletContext getAppletContext() {
        return cntx;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/04/2003 11:41:36 AM)
     *
     * @return java.lang.String
     */
    public java.lang.String getBaseUrl() {
        return baseUrl;
    }

    /**
     * Insert the method's description here.
     * Creation date: (15/04/2003 11:52:33 AM)
     *
     * @return java.applet.AppletContext
     */
    public java.applet.AppletContext getCntx() {
        return cntx;
    }

    /**
     * Insert the method's description here.
     * Creation date: (7/04/2003 10:28:13 AM)
     *
     * @return int
     */
    public int getDepthLimit() {
        return depthLimit;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/05/2003 3:31:47 PM)
     *
     * @return vlum.graph.GraphModel
     */
    public vlum.graph.GraphModel getGraphModel() {
        return graphModel;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/04/2003 11:41:36 AM)
     *
     * @return java.lang.String
     */
    public java.lang.String getHelpUrl() {
        return helpUrl;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/04/2003 11:41:36 AM)
     *
     * @return java.lang.String
     */
    public int getHistorySize() {
        return history.size();
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/04/2003 11:41:36 AM)
     *
     * @return java.lang.String
     */
    public java.lang.String getInitialTitle() {
        return initialTitle;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/05/2003 3:31:47 PM)
     *
     * @return vlum.graph.GraphSetup
     */
    public vlum.graph.GraphSetup getSetup() {
        return setup;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/04/2003 11:41:36 AM)
     *
     * @return java.lang.String
     */
    public java.lang.String getSetupUrl() {
        return setupUrl;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/04/2003 11:41:36 AM)
     *
     * @return java.lang.String
     */
    public java.lang.String getUserUrl() {
        return userUrl;
    }

    /**
     * Pop an item on top of the history stack.
     * Creation date: (15/04/2003 11:31:23 AM)
     */
    public int popHistoryItem() {
        if (history.size() > 1) {
            int index = Integer.parseInt((String) history.elementAt(history.size() - 2));
            addClickstream("BACK[" + graphModel.getTitle(index) + "]");
            history.removeElementAt(history.size() - 1);
            return index;
        }
        return -1;
    }

    /**
     * Push an item on top of the history stack.
     * Creation date: (15/04/2003 11:31:23 AM)
     */
    public void pushHistoryItem(String item) {
        history.addElement(item);
        addClickstream(graphModel.getTitle(item));
        graphModel.getTitle(item);
    }

    /**
     * Insert the method's description here.
     * Creation date: (15/04/2003 11:52:33 AM)
     *
     * @param newCntx java.applet.AppletContext
     */
    public void setAppletContext(java.applet.AppletContext newCntx) {
        cntx = newCntx;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/04/2003 11:41:36 AM)
     *
     * @param newBaseUrl java.lang.String
     */
    public void setBaseUrl(java.lang.String newBaseUrl) {
        baseUrl = newBaseUrl;
    }

    /**
     * Insert the method's description here.
     * Creation date: (7/04/2003 10:28:13 AM)
     *
     * @param newDepthLimit int
     */
    public void setDepthLimit(int newDepthLimit) {
        depthLimit = newDepthLimit;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/05/2003 3:31:47 PM)
     *
     * @param newGraphModel vlum.graph.GraphModel
     */
    public void setGraphModel(GraphModel newGraphModel) {
        graphModel = newGraphModel;
    }

    /**
     * Insert the method's description here.
     *
     * @param newGraphModel vlum.graph.GraphModel
     */
    public void initialiseGraphOverlay() {
        if (colourUrl.equals("") == false) {
            graphModel.setOverlayColour(colourUrl);
        }
        if (horizontalUrl.equals("") == false) {
            graphModel.setOverlayHorizontal(horizontalUrl);
        }
        graphModel.setOverlayInference(null);
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/04/2003 11:41:36 AM)
     *
     * @param newHelpUrl java.lang.String
     */
    public void setHelpUrl(java.lang.String newHelpUrl) {
        helpUrl = newHelpUrl;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/04/2003 11:41:36 AM)
     *
     * @param newInitialTitle java.lang.String
     */
    public void setInitialTitle(java.lang.String newInitialTitle) {
        initialTitle = newInitialTitle;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/05/2003 3:31:47 PM)
     *
     * @param newSetup vlum.graph.GraphSetup
     */
    public void setSetup(vlum.graph.GraphSetup newSetup) {
        setup = newSetup;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/04/2003 11:41:36 AM)
     *
     * @param newSetupUrl java.lang.String
     */
    public void setSetupUrl(java.lang.String newSetupUrl) {
        setupUrl = newSetupUrl;
    }

    /**
     * Insert the method's description here.
     * Creation date: (4/04/2003 11:41:36 AM)
     *
     * @param newUserUrl java.lang.String
     */
    public void setUserUrl(java.lang.String newUserUrl) {
        userUrl = newUserUrl;
    }

    /**
     * @return
     */
    public String getOntologyUrl() {
        return ontologyUrl;
    }

    /**
     * @param string
     */
    public void setOntologyUrl(String string) {
        ontologyUrl = string;
    }

    /**
     * @return
     */
    public String getClickstream() {
        return clickstream;
    }

    /**
     * @param
     */
    public void resetClickstream() {
        clickstream = "";
    }

    /**
     * @param string
     */
    public void addClickstream(String string) {
        clickstream = clickstream + ":::" + string + "(" + timestamp() + ")";
    }

    protected String timestamp() {
        Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss a");
        return formatter.format(now.getTime());
    }

    /**
     * clear the graph overlay.
     */
    public void clearOverlayColour() {
        graphModel.getOverlayColour().clear();
    }

    /**
     * apply a graph overlay.
     *
     * @param uri the location of the overlay.
     */
    public void applyOverlayColour() {
        graphModel.getOverlayColour().parse(colourUrl);
    }

    /**
     * clear the horizontal graph overlay.
     */
    public void clearOverlayHorizontal() {
        graphModel.getOverlayHorizontal().clear();
    }

    /**
     * apply a graph overlay.
     *
     * @param uri the location of the overlay.
     */
    public void applyOverlayHorizontal() {
        if (getHorizontalUrl().equals("") == false) {
            graphModel.getOverlayHorizontal().parse(horizontalUrl);
        }
    }

    /**
     * apply a graph overlay to a single element.
     *
     * @param uri the location of the overlay.
     */
    public void applyOverlayHorizontalSingle(String title) {
        //if (getHorizontalUrl().equals("") == false) {
        graphModel.getOverlayHorizontal().changeSingle(title);
        //}
    }


    /**
     * remove a graph overlay to a single element.
     *
     * @param uri the location of the overlay.
     */
    public void removeOverlayHorizontalSingle(String title) {
        if (getHorizontalUrl().equals("") == false) {
            graphModel.getOverlayHorizontal().revertSingle(title);
        }
    }

    /**
     * @return
     */
    public String getColourUrl() {
        return colourUrl;
    }

    /**
     * @return
     */
    public String getHorizontalUrl() {
        return horizontalUrl;
    }

    /**
     * @param string
     */
    public void setColourUrl(String string) {
        colourUrl = string;
    }

    /**
     * @param string
     */
    public void setHorizontalUrl(String string) {
        horizontalUrl = string;
    }

    /**
     * @return
     */
    public GraphOverlayColour getGraphOverlay() {
        return graphModel.getOverlayColour();
    }

    public int[] getSelected() {
        return graphModel.getOverlayHorizontal().getSelected();
    }

    public void applyOverlayInference(String title, int depth) {
        graphModel.getOverlayInference().doInference(title, depth);
    }

    public int getInferenceDepth(String title) {
        return graphModel.getOverlayInference().getInferenceDepth(title);
    }

    public float getInferenceDelta(String title) {
        return graphModel.getOverlayInference().getInferenceDelta(title);
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getContentBaseUrl() {
        return contentBaseUrl;
    }

    public void setContentBaseUrl(String contentBaseUrl) {
        this.contentBaseUrl = contentBaseUrl;
    }

    public void changeContentPane(String title) {
        AppletContext cntx = getAppletContext();
        try {
            String myUser = getUser();
            String myTitle = title;
            String myDepth = new Integer(getInferenceDepth(title)).toString();
            String myDelta = "-1";
            if (getInferenceDelta(title) != -1) {
                myDelta = new Float(getInferenceDelta(title)).toString();
            }
            String params = "?login=" + myUser + "&component=" + myTitle + "&delta=" + myDelta + "&depth=" + myDepth;
            System.out.println(getContentBaseUrl() + params);
            URL show = new URL(getContentBaseUrl() + params);
            //cntx.showDocument(show,"topicframe");
            //executeJS();
        } catch (MalformedURLException mfe) {
            mfe.printStackTrace();
        }
    }

    public String getHTMLConceptSummary(String title) {
        GenericGraphNode myNode = graphModel.getNodeObject(title);
        String summary = "";

        // title
        summary = summary + "<h2>" + title + "</h2>";

        // definition
        summary = summary + "<h3>Definition</h3><p>" + myNode.getDefinition() + "</p>";

        // peers
        summary = summary + "<h3>Relationships</h3><p>";
        Vector peerVector = myNode.getPeers();
        for (int i = 0; i < peerVector.size(); i++) {
            GenericGraphPeer myPeer = ((GenericGraphPeer) peerVector.elementAt(i));
            String relatedName = "<a href=\"javascript:select('" + graphModel.getTitle(graphModel.getAboutIndex(myPeer.getEndNode())) + "');getContent()\">" + graphModel.getTitle(graphModel.getAboutIndex(myPeer.getEndNode())) + "</a>";
            String relatedDesc = myPeer.getDescription();
            String relatedType = myPeer.getStrength() + " " + myPeer.getType();
            summary = summary + relatedName + " is <b>" + relatedDesc + "</b> (" + relatedType + ")<br>";
            String sourceName = "<a href=\"javascript:select('" + graphModel.getTitle(graphModel.getAboutIndex(myPeer.getSource())) + "');getContent()\">" + graphModel.getTitle(graphModel.getAboutIndex(myPeer.getSource())) + "</a>";
            summary = summary + "&nbsp;&nbsp;&nbsp;&nbsp;Mecureo decided this through the keyword <i>" + myPeer.getKeyword() + "</i> from the definition of " + sourceName + ".";
            summary = summary + "<br><br>";
        }
        summary = summary + "</p>";

        return summary;
    }
}
