
import vlum.ContextData;
import vlum.display.*;
import vlum.graph.model.OWLQDXMLModel;
import vlum.graph.model.OWLQDXMLSetup;
import vlum.graph.model.SKOSQDXMLModel;
import vlum.graph.model.SKOSQDXMLSetup;

import javax.swing.*;
import java.applet.AppletContext;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Hashtable;

public class squidge extends JApplet implements ActionListener {

    public static final long serialVersionUID = 3L;

    private static void emit(String s) {
        System.out.println(s);
    }

    /**
     * main entrypoint - starts the part when it is run as an application
     *
     * @param args java.lang.String[]
     */
    public static void main(java.lang.String[] args) {
        try {
            javax.swing.JFrame frame = new javax.swing.JFrame();
            squidge aTestSetupApplet;
            Class iiCls = Class.forName("squidge");
            ClassLoader iiClsLoader = iiCls.getClassLoader();
            aTestSetupApplet = (squidge) java.beans.Beans.instantiate(
                    iiClsLoader, "squidge");
            frame.getContentPane().add("Center", aTestSetupApplet);
            frame.setSize(350, 700);
            frame.addWindowListener(new java.awt.event.WindowAdapter() {
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                };
            });
            frame.show();
            //java.awt.Insets insets = frame.getInsets();
            frame.setVisible(true);
        } catch (Throwable exception) {
            System.err.println("Exception occurred in main() of javax.swing.JApplet");
            exception.printStackTrace(System.out);
        }
    }

    // the menus and menu items
    private JMenuItem backMenuItem;

    private int currentDisplayIndex;

    //graph and data fields
    private String currentTag;

    private String currentType;

    private JLabel depthLabel;

    // the depth slider
    private JSlider depthSlider;

    private Hashtable generalSetup;

    //display metadata
    private Hashtable menuItemNames;

    private JMenuItem mkMenuItem;

    private ActionMenu myActionMenu;

    private ActionPopupMenu myActionPopupMenu;

    private ContextData myContextData;

    private DefaultSelectPane myDefaultSelectPane;

    private DrawMenu myDrawMenu;

    private FilterMenu myFilterMenu;

    private HelpMenu myHelpMenu;

    //display fields
    //private StandardSlider myStandardSlider;
    //private DepthSlider myDepthSlider;
    //private DefaultPane myDefaultPane;b
    private SquidgePane mySquidgePane;

    private JMenuItem resetMenuItem;

    // the display pane
    private SquidgePane squidgePane;

    private JLabel viewLabel;

    //web front end
    //private FrontEnd myFrontEnd;

    private void executeJS() {
        String jscmd = "getContent()";  /* JavaScript command */
        try {
            Method getw = null, eval = null;
            Object jswin = null;
            Class c = Class.forName("netscape.javascript.JSObject"); /* does it in IE too */
            Method ms[] = c.getMethods();
            for (int i = 0; i < ms.length; i++) {
                if (ms[i].getName().compareTo("getWindow") == 0)
                    getw = ms[i];
                else if (ms[i].getName().compareTo("eval") == 0)
                    eval = ms[i];
            }
            Object a[] = new Object[1];
            a[0] = this;               /* this is the applet */
            jswin = getw.invoke(c, a); /* this yields the JSObject */
            a[0] = jscmd;
            eval.invoke(jswin, a);
        } catch (InvocationTargetException ite) {
            System.out.println("Can't invoke Javascript method getContent()");
            System.out.println(myContextData.getHTMLConceptSummary(getCurrentTitle()));

        } catch (Exception e) {
            System.out.println("Can't retrieve JSObject from current context");
            System.out.println(myContextData.getHTMLConceptSummary(getCurrentTitle()));
        }
    }


    public void actionPerformed(ActionEvent e) {
        //Image i = getImage(getCodeBase(), "n");
        Object source = e.getSource();
        String t = null;
        try {
            if (source.getClass() == Class.forName("javax.swing.JMenuItem"))
                t = ((JMenuItem) source).getText();
            else
                t = e.getActionCommand();
        } catch (ClassNotFoundException ex) {
            t = e.getActionCommand();
        }
        AppletContext cntx = getAppletContext();
        repaint();

        // mouse click on element
        if (t.length() >= 21 && t.startsWith("SquidgePaneShowString")) {
            cntx.showStatus(t.substring(21, t.length()));
        }
        if (t.length() >= 17 && t.startsWith("SquidgePaneClick:")) {
            myContextData.pushHistoryItem(t.substring(17, t.length()));
            if (myContextData.getHistorySize() > 1) {
                myActionMenu.enableBack();
            }
        }
        if (t.length() >= 17 && t.startsWith("changeContentPane")) {
            executeJS();
            //myContextData.changeContentPane(getCurrentTitle());
        }
    }

    private void changeToView(int index) {

        currentDisplayIndex = index;

        //myStandardSlider.changeValue(50);

        squidgePane.changeDisplay(currentDisplayIndex);

        viewLabel.setToolTipText(myContextData.getSetup().getNodeToolTip(
                currentDisplayIndex));
        viewLabel.setText("Viewing "
                + myContextData.getSetup().getNodeTag(currentDisplayIndex));
    }

    public String getClickstream() {
        return myContextData.getClickstream();
    }

    public String getCurrentColourOverlay() {
        return myContextData.getGraphOverlay().getOverlayURI();
    }

    /**
     * Insert the method's description here. Creation date: (30/04/2003 5:31:57
     * PM)
     */
    public String getCurrentTitle() {
        return mySquidgePane.getCurrentSelectedTitle();
    }

    public float getInferenceDelta() {
        return myContextData.getInferenceDelta(getCurrentTitle());
    }

    public int getInferenceDepth() {
        return myContextData.getInferenceDepth(getCurrentTitle());
    }

    /**
     * Go back one. Creation date: (4/05/2003 2:32:22 PM)
     */
    public void goBackOne() {
        if (myContextData.getHistorySize() > 1) {
            mySquidgePane.setSelectedTopic(myContextData.popHistoryItem());
        }
    }

    public void init() {
        emit("Starting SIV 3.5...");
        emit("Loading datafiles");
        myContextData = new ContextData();
        myContextData.setAppletContext(getAppletContext());

        // load initial parameters
        //loadParameters("hci");
        loadParameters("skos");

        JPanel contentPane = new JPanel();
        //contentPane.setBackground(Color.white);
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        setContentPane(contentPane);

        JMenuBar menuBar = new JMenuBar();
        //setJMenuBar(menuBar);
        JMenuItem menuItem;

        menuItemNames = new Hashtable();
        int menuItemCount = 0;

        // setup the display menu
        JMenu displayMenu = new JMenu("Display");
        menuBar.add(displayMenu);
        for (int i = 0; i < myContextData.getSetup().displaysLength(); i++) {
            if (myContextData.getSetup().isViewable(i)) {
                menuItem = new JMenuItem(myContextData.getSetup().getNodeTag(i));
                menuItem.addActionListener(this);
                displayMenu.add(menuItem);
                menuItemNames.put(myContextData.getSetup().getNodeTag(i),
                        new Integer(-1));
                //-1
            }
        }
        if (myContextData.getSetup().displaysLength() <= 1) {
            displayMenu.setEnabled(false);
        }

        squidgePane = new SquidgePane();
        squidgePane.setAnimate(true);
        squidgePane.setDisplayIndex(currentDisplayIndex);
        squidgePane.setRelationshipFilter(null);
        setDraggable(true);
        squidgePane.addActionListener(this);
        squidgePane.setDepthLimit(2);

        viewLabel = new JLabel();

        // test control pane class
        myActionMenu = new ActionMenu(squidgePane, myContextData);
        myDrawMenu = new DrawMenu(squidgePane);
        myFilterMenu = new FilterMenu(squidgePane, myContextData);
        myHelpMenu = new HelpMenu(squidgePane, myContextData);

        myDefaultSelectPane = new DefaultSelectPane(myContextData.getSetup(),
                currentDisplayIndex, contentPane, squidgePane, myContextData);
        //myDefaultPane = new DefaultPane(myContextData.getSetup(),
        // currentDisplayIndex, contentPane, squidgePane, myContextData);
        //myStandardSlider = new StandardSlider(myContextData.getSetup(),
        // currentDisplayIndex, contentPane, squidgePane);
        //myDepthSlider = new DepthSlider(contentPane, squidgePane);
        //myActionPopupMenu = new ActionPopupMenu(squidgePane, myContextData);
        DepthRadioButtons myDepthRadioButtons = new DepthRadioButtons(
                contentPane, squidgePane);
        myDepthRadioButtons.setContext(myContextData);

        menuBar.add(myActionMenu.getMenu());
        menuBar.add(myDrawMenu.getMenu());
        menuBar.add(myFilterMenu.getMenu());
        menuBar.add(myHelpMenu.getMenu());

        mySquidgePane = squidgePane;
        contentPane.add(mySquidgePane);
        this.repaint();

        //myDepthSlider.setDepthLimit(2);
        //myDepthRadioButtons.setDepthLimit(2);
        //myDepthRadioButtons.setContext(myContextData);
        loadDataSet(myContextData.getOntologyUrl());
        //myFrontEnd = new FrontEnd(myContextData, squidgePane);
        mySquidgePane.setSelectedTopic(mySquidgePane.getSelectedTopic());
    }

    private void loadDataSet(String url) {

        try {
            emit("Model is: " + url);
            if (url.endsWith(".owl")) {
                myContextData.setGraphModel(new OWLQDXMLModel(myContextData.getSetup(), url, this));
            }
            if (url.endsWith(".skos")) {
                myContextData.setGraphModel(new SKOSQDXMLModel(myContextData.getSetup(), url, this));
            }
            myContextData.initialiseGraphOverlay();
        } catch (Exception e) {
            emit("Warning - EXCEPTION on loading: ");
            e.printStackTrace();
        }

        squidgePane.setGraphModel(myContextData.getGraphModel());
        int cm;
        if (myContextData.getInitialTitle() == null)
            cm = 0;
        else
            cm = myContextData.getGraphModel().getTitleIndex(
                    myContextData.getInitialTitle());
        myContextData.pushHistoryItem("" + cm);
        squidgePane.setSelectedTopic(cm);
        this.changeToView(0);

    }

    /**
     * Load up parameters (additional ones for testing purposes). Creation date:
     * (3/26/02 10:39:21 AM)
     */
    private void loadParameters(String myParams) {
        // load base Url
        myContextData.setBaseUrl(this.getCodeBase().toString());
        emit("BaseURL is " + myContextData.getBaseUrl());
        try {
            if ((getParameter("ontologyUrl")) == null) {
                throw (new Exception());
            }
            // default in browser
            try {
                myContextData.setDepthLimit(new Integer(
                        getParameter("depthLimit")).intValue());
            } catch (Exception e) {
                // depth limit is not set
                myContextData.setDepthLimit(-1);
            }
            try {
                myContextData.setUser(getParameter("user"));
            } catch (Exception e) {
            }
            try {
                myContextData.setContentBaseUrl(getParameter("contentBaseUrl"));
            } catch (Exception e) {
            }
            myContextData.setInitialTitle(getParameter("initialTitle"));
            myContextData.setOntologyUrl(getParameter("ontologyUrl"));
            if (getParameter("colourUrl") != null) {
                myContextData.setColourUrl(getParameter("colourUrl"));
            }
            if (getParameter("horizontalUrl") != null) {
                myContextData.setHorizontalUrl(getParameter("horizontalUrl"));
            }
        } catch (Exception e) {
            // out of browser loading for testing
            if (myParams.equals("hci")) {
                myContextData.setInitialTitle("usability");
                myContextData.setOntologyUrl("http://www.it.usyd.edu.au/~uidp/ontologies/uidp.owl");
                myContextData.setColourUrl("http://www.it.usyd.edu.au/~uidp/personis/results.cgi?login=alum");
                myContextData.setHorizontalUrl("http://www.it.usyd.edu.au/~uidp/personis/overlay-importance.xml");
            } else if (myParams.equals("skos")) {
                myContextData.setInitialTitle("Arrays");
                myContextData.setOntologyUrl("http://rp-www.cs.usyd.edu.au/~alum/ontologies/soft2130.skos");
            }
        }
        if (myContextData.getOntologyUrl().endsWith(".owl")) {
            myContextData.setSetup(new OWLQDXMLSetup(myContextData.getOntologyUrl()));
        }
        if (myContextData.getOntologyUrl().endsWith(".skos")) {
            myContextData.setSetup(new SKOSQDXMLSetup(myContextData.getOntologyUrl()));
        }

    }

    public void performInference() {
        myContextData.applyOverlayInference(mySquidgePane
                .getCurrentSelectedTitle(), mySquidgePane.getDepthLimit());
        mySquidgePane.setSelectedTopic(mySquidgePane.getCurrentSelected());
    }

    public void reloadSquidge() {
        loadDataSet(myContextData.getUserUrl());
    }

    public void reloadSquidge(String ab) {
        emit("Reloading with new title " + ab);
        myContextData.setInitialTitle(myContextData.getGraphModel().getTitle(
                myContextData.getGraphModel().getAboutIndex(ab)));
        loadDataSet(myContextData.getUserUrl());
    }

    public void reloadSquidgeWithTitle(String ab) {
        emit("Reloading with new title " + ab);
        myContextData.setInitialTitle(myContextData.getGraphModel().getTitle(
                myContextData.getGraphModel().getAboutIndex(ab)));
        loadDataSet(myContextData.getUserUrl());
    }

    public void resetClickstream() {
        myContextData.resetClickstream();
    }

    public void resetSquidge() {
        this.changeToView(0);
        int cm;
        if (myContextData.getInitialTitle() == null)
            cm = 0;
        else
            cm = myContextData.getGraphModel().getTitleIndex(
                    myContextData.getInitialTitle());
        myContextData.pushHistoryItem("" + cm);
        myFilterMenu.selectNone();
        squidgePane.setRelationshipFilter(null);
        squidgePane.setSelectedTopic(cm);
        backMenuItem.setEnabled(false);
    }

    /**
     * Search for a title. Creation date: (4/05/2003 2:32:22 PM)
     */
    public void searchTitles(String regex) {
        mySquidgePane.searchFor(regex);
        myContextData.addClickstream("SEARCH[" + regex + "]");
    }

    public void selectItem(String s) {
        int cm = -1;
        try {
            cm = myContextData.getGraphModel().getAboutIndex(s);
        } catch (Exception ex) {
            return;
        }
        if (cm != -1)
            squidgePane.setSelectedTopic(cm);
    }

    public void selectTitle(String s) {
        int cm = -1;
        try {
            cm = myContextData.getGraphModel().getTitleIndex(s);
        } catch (Exception ex) {
            return;
        }
        if (cm != -1)
            mySquidgePane.setSelectedTopic(cm);
    }

    public void setDraggable(boolean b) {
        squidgePane.setDragable(b);
    }

    public void setOverlayColour(String uri) {
        mySquidgePane.setLoading(true);
        myContextData.clearOverlayColour();
        myContextData.setColourUrl(uri);
        myContextData.applyOverlayColour();
        mySquidgePane.setLoading(false);
        mySquidgePane.setSelectedTopic(mySquidgePane.getSelectedTopic());
    }

    public void setOverlayHorizontal(String uri) {
        myContextData.clearOverlayHorizontal();
        myContextData.setHorizontalUrl(uri);
        myContextData.applyOverlayHorizontal();
        mySquidgePane.setSelectedTopic(mySquidgePane.getSelectedTopic());
    }

    /**
     * Set the loading screen to be displayed. If it is already displayed, then
     * un-display it.
     */
    public void setLoading() {
        if (mySquidgePane.isLoading()) {
            mySquidgePane.setLoading(false);
        } else {
            mySquidgePane.setLoading(true);
        }
        mySquidgePane.setSelectedTopic(mySquidgePane.getCurrentSelected());
    }

    public String getHTMLConceptSummary(String title) {
        return myContextData.getHTMLConceptSummary(title);
    }
}