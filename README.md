LOSUM-SIV
=========

This project was the topic for my PhD thesis. The core ideas of this work is the integration of light-weight ontologies into user models to provide structure and reasoning. This project was funded by Hewlett-Packard. I was supervised by Judy Kay, and co-supervised by Claudio Bartolini (HP Labs, Palo Alto).

More Information: http://rp-www.it.usyd.edu.au/~alum/index.cgi?Losum


To build:
---------
gradle build


To run:
-------
squidge is the application entry point. Include the lib/ directory in your classpath.